// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpawnVolume.h"
#include "SimpleStealthGame/NPC/Enemy.h"
#include "EnemySpawnVolume.generated.h"

/**
 * Enemies spawning volume
 */
UCLASS()
class SIMPLESTEALTHGAME_API AEnemySpawnVolume : public ASpawnVolume
{
	GENERATED_BODY()

private:
	/* Enemy class to be used in this spawner */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AEnemy> EnemyToSpawn;

	/* Player spawn location to check when spawning enemies */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	FVector PlayerSpawnLocation;
	
private:
	/* Spawn the enemy */
	UFUNCTION(BlueprintCallable, Category = Spawning)
	class AActor* SpawnActor() override;

	/* Is spawn safe for player (enemy is not too close) */
	bool IsSafeSpawn(const FVector& SpawnedActorViewLocation, const float& Distance) const;

	/* Destroy the spawned enemy */
	bool DestroySpawned(AActor* Spawned) const;

protected:
	void BeginPlay() override;

public:
	AEnemySpawnVolume();
	
	/* Set map's player spawn point location */
	UFUNCTION(BlueprintCallable, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	void SetPlayerSpawnLocation(const FVector& LocationToSet);

	/* Registers the enemy actor that will be used for spawning */
	void RegisterSpawnActor() override;
};
