// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnVolume.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "AIController.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASpawnVolume::ASpawnVolume()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawningBox = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawningBox"));
	InitDefaults();
}

void ASpawnVolume::InitDefaults()
{
	ReSpawnTriesCount = 100;	
	SpawnedActorsCount = 0;

	SpawnParameters.Template = NULL;
	SpawnParameters.Owner = NULL;
	SpawnParameters.Instigator = NULL;
	SpawnParameters.OverrideLevel = NULL;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
}

// Called when the game starts or when spawned
void ASpawnVolume::BeginPlay()
{ 
	Super::BeginPlay();	
}

// Called every frame
void ASpawnVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FVector ASpawnVolume::GetSpawnPoint() const
{
	const auto Extent = SpawningBox->GetScaledBoxExtent();
	const auto Origin = SpawningBox->GetComponentLocation();
	const auto Point = UKismetMathLibrary::RandomPointInBoundingBox(Origin, Extent);	

	return Point;
}

TSubclassOf<AActor> ASpawnVolume::GetSpawnActor() const
{
	if (!IsValid(ActorToSpawn)) return nullptr;

	return ActorToSpawn;
}

void ASpawnVolume::SetSpawnActor(TSubclassOf<AActor> ActorToSet)
{
	if (IsValid(ActorToSet))
	{
		ActorToSpawn = ActorToSet;
	}
	else
	{
		ULogger::LogError("SpawnActor. SetSpawnActor. Can's set an invalid spawn actor!");		
	}
}

AActor* ASpawnVolume::SpawnActor()
{
	if (!IsValid(ActorToSpawn)) return nullptr;	

	auto World = GetWorld();
	if (!IsValid(World)) return nullptr;
	
	auto LocationToSpawn = GetSpawnPoint();
	const auto ToSpawnName = ActorToSpawn->GetName();	

	AActor* Spawned = nullptr;
	auto CurrentReSpawnTryNum = 0;	

	while (!IsValid(Spawned) && CurrentReSpawnTryNum < ReSpawnTriesCount)
	{		
		++CurrentReSpawnTryNum;		
		LocationToSpawn = GetSpawnPoint();
		Spawned = World->SpawnActor<AActor>(ActorToSpawn, LocationToSpawn, FRotator(.0F), SpawnParameters);
	}
	
	if (!IsValid(Spawned))
	{		
		ULogger::LogWarning(FString::Printf(TEXT("SpawnVolume. SpawnActor. Cannot spawn actor of type %s, please check UClass to spawn, spawn volume's size and overlapping spawn volumes."), *ToSpawnName));
		return nullptr;
	}

	return Spawned;
}

bool ASpawnVolume::SpawnAllActors(UMaterialInterface* Material)
{	
	for (int i = 0; i < ActorsToSpawnCount; i++)
	{	
		const auto Spawned = SpawnActor();
		if (!IsValid(Spawned))
		{
			ULogger::LogError("SpawnVolume. SpawnAllActors. Spawning failed.");
			return false;
		}

		TArray<UStaticMeshComponent*> MaterialComponents;
		Spawned->GetComponents(MaterialComponents);

		for (int j = 0; j < MaterialComponents.Num(); j++)
		{
			auto TargetComponent = MaterialComponents[j];
			const auto MaterialsCount = TargetComponent->GetNumMaterials();

			for (int k = 0; k < MaterialsCount; k++)
			{
				TargetComponent->SetMaterial(k, Material);
			}
		}		
	}

	return true;
}

void ASpawnVolume::RegisterSpawnActor() { } // no logic here, imitating pure virtual method
