// Fill out your copyright notice in the Description page of Project Settings.


#include "ObstacleSpawnVolume.h"
#include "SimpleStealthGame/Environment/Obstacle.h"

AObstacleSpawnVolume::AObstacleSpawnVolume() { }

void AObstacleSpawnVolume::BeginPlay() { }

AActor* AObstacleSpawnVolume::SpawnActor()
{
	return Super::SpawnActor();
}

void AObstacleSpawnVolume::RegisterSpawnActor()
{
	SetSpawnActor(ObstacleToSpawn);
}
