// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WinSpot.generated.h"

/*
 * A spot that, once reached, makes player win the game
 */
UCLASS()
class SIMPLESTEALTHGAME_API AWinSpot : public AActor
{
	GENERATED_BODY()

private:
	/* A rectangular volume which should toggle player's victory upon overlapping */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Transition, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* TransitionVolume;

	/* Mesh component to hold the material of volume */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;

	/* A billboard for the editor */
	class UBillboardComponent* BillboardComponent;

public:
	// Sets default values for this actor's properties
	AWinSpot();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/* Events occured on start colliding (overlapping) with item */
	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                            AActor* OtherActor,
	                            UPrimitiveComponent* OtherComp,
	                            int32 OtherBodyIndex,
	                            bool bFromSweep,
	                            const FHitResult& SweepResult);
};
