// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemSpawnVolume.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"

AItemSpawnVolume::AItemSpawnVolume() { }

void AItemSpawnVolume::BeginPlay() { }

void AItemSpawnVolume::RegisterSpawnActor()
{
	SetSpawnActor(ItemToSpawn);
}

AActor* AItemSpawnVolume::SpawnActor()
{
	const auto Spawned = Super::SpawnActor();

	auto Item = Cast<AItem>(Spawned);
	if (IsValid(Item))
	{
		Item->SetInitialLocation(Spawned->GetActorLocation());
		return Spawned;
	}
	
	return nullptr;	
}

