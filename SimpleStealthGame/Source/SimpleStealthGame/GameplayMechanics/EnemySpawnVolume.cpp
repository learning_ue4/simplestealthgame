// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawnVolume.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"
#include "SimpleStealthGame/NPC/EnemyAIController.h"
#include "Kismet/GameplayStatics.h"

AEnemySpawnVolume::AEnemySpawnVolume()
{	
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
}

void AEnemySpawnVolume::BeginPlay() { }

AActor* AEnemySpawnVolume::SpawnActor()
{
	if (!IsValid(ActorToSpawn)) return nullptr;

	auto World = GetWorld();
	if (!IsValid(World)) return nullptr;

	auto LocationToSpawn = GetSpawnPoint();
	const auto ToSpawnName = ActorToSpawn->GetName();

	AActor* Spawned = nullptr;
	auto CurrentReSpawnTryNum = 0;

	while (!IsValid(Spawned) && CurrentReSpawnTryNum < ReSpawnTriesCount)
	{
		++CurrentReSpawnTryNum;
		LocationToSpawn = GetSpawnPoint();
		Spawned = World->SpawnActor<AActor>(ActorToSpawn, LocationToSpawn, FRotator(.0F), SpawnParameters);

		if (!IsValid(Spawned))
		{
			ULogger::LogWarning(FString::Printf(TEXT("SpawnVolume. SpawnActor. Can't spawn actor of type %s, please check UClass to spawn, spawn volume's size and overlapping spawn volumes."), *ToSpawnName));
			return nullptr;
		}

		auto Enemy = Cast<AEnemy>(Spawned);
		
		if (!IsValid(Enemy))
		{
			const auto bIsDestructionSuccessful = DestroySpawned(Enemy);
			if (!bIsDestructionSuccessful)
			{
				ULogger::LogError("EnemySpawnVolume. SpawnActor. Spawned actor is not an enemy, but it can't be destroyed. Level restart needed.");
				return nullptr;
			}
		}

		Enemy->SpawnDefaultController();

		const auto SpawnedEnemyAICtrl = Cast<AEnemyAIController>(Enemy->GetController());
		if (!IsValid(SpawnedEnemyAICtrl))
		{			
			const auto bIsDestructionSuccessful = DestroySpawned(Enemy);
			if (!bIsDestructionSuccessful)
			{
				ULogger::LogError("EnemySpawnVolume. SpawnActor. Enemy: can't properly cast a controller to EnemyAIController, spawned enemy is disabled, but can't be destroyed. Level restart needed.");
				return nullptr;
			}
		}

		Enemy->SetAIController(SpawnedEnemyAICtrl);
		const auto EnemyViewLocation = Enemy->GetPawnViewLocation();
		const auto bIsSafeSpawn = IsSafeSpawn(EnemyViewLocation, Enemy->GetSightDistanceCm());

		if (!bIsSafeSpawn)
		{
			const auto bIsDestructionSuccessful = DestroySpawned(Enemy);
			if (!bIsDestructionSuccessful)
			{
				ULogger::LogError("EnemySpawnVolume. SpawnActor. Enemy spawned too close to the player, but it can't be destroyed. Level restart needed.");
				return nullptr;
			}			
		}
	}
	return Spawned;
}

void AEnemySpawnVolume::SetPlayerSpawnLocation(const FVector& LocationToSet)
{
	PlayerSpawnLocation = LocationToSet;
}

void AEnemySpawnVolume::RegisterSpawnActor()
{
	SetSpawnActor(EnemyToSpawn);
}

bool AEnemySpawnVolume::IsSafeSpawn(const FVector& SpawnedActorViewLocation, const float& Distance) const
{
	const auto PlayerLocationFromSpawnedActor = FVector(PlayerSpawnLocation - SpawnedActorViewLocation);
	const auto DistanceToPlayer = PlayerLocationFromSpawnedActor.Size();

	return DistanceToPlayer > Distance;
}

bool AEnemySpawnVolume::DestroySpawned(AActor* Spawned) const
{
	const auto World = GetWorld();
	if (!IsValid(World))
	{
		ULogger::LogError("EnemySpawnVolume. DestroySpawned. Can't locate World context, incorrectly spawned enemy could not be destroyed!");
		return false;
	}	

	return World->DestroyActor(Spawned);
}
