// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpawnVolume.h"
#include "SimpleStealthGame/Items/Item.h"
#include "ItemSpawnVolume.generated.h"

/**
 * Items spawning volume
 */
UCLASS()
class SIMPLESTEALTHGAME_API AItemSpawnVolume : public ASpawnVolume
{
	GENERATED_BODY()	

private:
	/* Item class to be used in this spawner */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AItem> ItemToSpawn;

private:
	/* Spawn the item */
	UFUNCTION(BlueprintCallable, Category = Spawning)
	class AActor* SpawnActor() override;

protected:
	void BeginPlay() override;

public:
	AItemSpawnVolume();

	/* Registers the enemy actor that will be used for spawning */
	void RegisterSpawnActor() override;
};
