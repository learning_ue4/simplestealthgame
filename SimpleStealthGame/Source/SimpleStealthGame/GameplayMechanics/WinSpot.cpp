// Fill out your copyright notice in the Description page of Project Settings.

#include "WinSpot.h"
#include "Components/BoxComponent.h"
#include "Components/BillboardComponent.h"
#include "SimpleStealthGame/Player/MainCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "SimpleStealthGame/GameGeneral/SSGGameMode.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"

// Sets default values
AWinSpot::AWinSpot()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	TransitionVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("TransitionVolume"));
	RootComponent = TransitionVolume;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TransitionVolumeMesh"));
	MeshComponent->SetCanEverAffectNavigation(false);
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);	
	MeshComponent->SetupAttachment(RootComponent);

	BillboardComponent = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
	BillboardComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWinSpot::BeginPlay()
{
	Super::BeginPlay();

	TransitionVolume->OnComponentBeginOverlap.AddDynamic(this, &AWinSpot::OnOverlapBegin);	
}

// Called every frame
void AWinSpot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWinSpot::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (!IsValid(OtherActor)) return;

	const auto MainCharacter = Cast<AMainCharacter>(OtherActor);
	if (!IsValid(MainCharacter) || MainCharacter->IsDead()) return;

	const auto World = GetWorld();
	if (!IsValid(World))
	{
		ULogger::LogError("WinSpot. OnOverlapBegin. Can't get world context! Proper WinSpot functioning is impossible!");
		return;
	}

	auto GameMode = Cast<ASSGGameMode>(UGameplayStatics::GetGameMode(World));

	if (IsValid(GameMode))
	{
		ULogger::LogError("WinSpot. OnOverlapBegin. Can't get game mode! Proper WinSpot functioning is impossible!");
		return;
	}
	
	GameMode->SetGameOver(MainCharacter, true);
}
