// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpawnVolume.h"
#include "ObstacleSpawnVolume.generated.h"

/**
 * Obstacles spawning volume
 */
UCLASS()
class SIMPLESTEALTHGAME_API AObstacleSpawnVolume : public ASpawnVolume
{
	GENERATED_BODY()

private:
	/* Obstacle class to be used in this spawner, no specific class for the obstacle - using simple AActor */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AActor> ObstacleToSpawn;	

protected:
	void BeginPlay() override;
	
public:
	AObstacleSpawnVolume();
	
	/* Spawn the obstacle */
	UFUNCTION(BlueprintCallable, Category = Spawning)
	class AActor* SpawnActor() override;

	/* Registers the obstacle actor that will be used for spawning */
	void RegisterSpawnActor() override;
};
