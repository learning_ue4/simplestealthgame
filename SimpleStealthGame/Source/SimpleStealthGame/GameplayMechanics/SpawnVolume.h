// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnVolume.generated.h"

/*
 * Volume that spawns actors on level
 */
UCLASS(Abstract)
class SIMPLESTEALTHGAME_API ASpawnVolume : public AActor
{
	GENERATED_BODY()

protected:
	/* Spawn volume's borders */
	UPROPERTY(VisibleAnywhere, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* SpawningBox;

	/* Number of objects of selected type to spawn at the location */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	int32 ActorsToSpawnCount;

	/* Number of object of selected type actually spawned */
	int32 SpawnedActorsCount;

	/*
	 * A heuristic number of tries to spawn an actor at random location,
	 * considering the fact that selected location could be occupied by
	 * previously spawned actor. This can be calculated for a particular sphere,
	 * but we leave it hard-coded for prototyping purposes
	 */
	int32 ReSpawnTriesCount;
	
	/* Objects to spawn at this volume */
	UPROPERTY()
	TSubclassOf<AActor> ActorToSpawn;

	/* Parameters to be used for spawning a particular actor */
	FActorSpawnParameters SpawnParameters;	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* Initialize default parameters */
	void InitDefaults();

	/* A randomly generated point in a box where an object will be spawned */
	UFUNCTION(BlueprintPure, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	FVector GetSpawnPoint() const;

	/* Spawn one actor */
	virtual class AActor* SpawnActor();

	FORCEINLINE bool AreAllSpawned() const { return  ActorsToSpawnCount == SpawnedActorsCount; }

	/* Set actor to be spawned by the volume */
	void SetSpawnActor(TSubclassOf<AActor> ActorToSet);

public:
	// Sets default values for this actor's properties
	ASpawnVolume();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* A particular actor that is spawned by this box */
	UFUNCTION(Category = Spawning)
	TSubclassOf<AActor> GetSpawnActor() const;

	/* Spawn all set actors */
	UFUNCTION(BlueprintCallable, Category = Spawning)
	bool SpawnAllActors(class UMaterialInterface* Material);

	/* Pure virtual method to be used in child classes for registering the particular actor they are going to spawn */
	virtual void RegisterSpawnActor();
};
