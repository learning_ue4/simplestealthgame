// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SSGGameInstance.generated.h"

/*
 * Custom game instance to be used as main
 */
UCLASS()
class SIMPLESTEALTHGAME_API USSGGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	void Init() override;
};
