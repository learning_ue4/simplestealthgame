// Fill out your copyright notice in the Description page of Project Settings.


#include "SSGGameMode.h"
#include "EngineUtils.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"
#include "SimpleStealthGame/Player/MainCharacterController.h"
#include "SimpleStealthGame/Player/MainCharacter.h"
#include "TimerManager.h"
#include "SimpleStealthGame/GameplayMechanics/SpawnVolume.h"
#include "SimpleStealthGame/GameplayMechanics/ObstacleSpawnVolume.h"
#include "SimpleStealthGame/GameplayMechanics/ItemSpawnVolume.h"
#include "SimpleStealthGame/GameplayMechanics/EnemySpawnVolume.h"
#include "SimpleStealthGame/Settings/SSGGameUserSettings.h"

ASSGGameMode::ASSGGameMode()
{
	GameWonText = "YOU WIN! CONGRATULATIONS!!";
	GameLostText = "YOU'VE LOST! BETTER LUCK NEXT TIME!!";

	LevelRestartIntervalSec = 10;
	LevelRestartIntervalIncrementSec = 1;
	LevelRestartElapsedTimeSec = 0;
}

void ASSGGameMode::ApplyStyle()
{
	auto engineReference = GEngine;
	if (!IsValid(engineReference)) return;
	
	const auto Settings = Cast<USSGGameUserSettings>(engineReference->GetGameUserSettings());

	if (IsValid(Settings))
	{
		auto StyleId = Settings->GetStyleId();

		TArray<TArray<FString>> MaterialPaths;
		
		TArray<FString> ObstacleMaterialPaths;
		ObstacleMaterialPaths.Emplace("/Game/Materials/StyleOriginal/M_Obstacle_Original.M_Obstacle_Original");
		ObstacleMaterialPaths.Emplace("/Game/Materials/StyleDark/M_Obstacle_Dark.M_Obstacle_Dark");
		ObstacleMaterialPaths.Emplace("/Game/Materials/StyleInverted/M_Obstacle_Inverted.M_Obstacle_Inverted");
		ObstacleMaterialPaths.Emplace("/Game/Materials/StylePark/M_Obstacle_Park.M_Obstacle_Park");
		MaterialPaths.Emplace(ObstacleMaterialPaths);

		TArray<FString> EnemyMaterialPaths;
		EnemyMaterialPaths.Emplace("/Game/Materials/StyleOriginal/M_Bot_Original.M_Bot_Original");
		EnemyMaterialPaths.Emplace("/Game/Materials/StyleDark/M_Bot_Dark.M_Bot_Dark");
		EnemyMaterialPaths.Emplace("/Game/Materials/StyleInverted/M_Bot_Inverted.M_Bot_Inverted");
		EnemyMaterialPaths.Emplace("/Game/Materials/StylePark/M_Bot_Park.M_Bot_Park");
		MaterialPaths.Emplace(EnemyMaterialPaths);

		TArray<FString> FloorMaterialPaths;
		FloorMaterialPaths.Emplace("/Game/Materials/StyleOriginal/M_Floor_Original.M_Floor_Original");
		FloorMaterialPaths.Emplace("/Game/Materials/StyleDark/M_Floor_Dark.M_Floor_Dark");
		FloorMaterialPaths.Emplace("/Game/Materials/StyleInverted/M_Floor_Inverted.M_Floor_Inverted");
		FloorMaterialPaths.Emplace("/Game/Materials/StylePark/M_Floor_Park.M_Floor_Park");
		MaterialPaths.Emplace(FloorMaterialPaths);


		TArray<FString> ItemMaterialPaths;
		ItemMaterialPaths.Emplace("/Game/Materials/StyleOriginal/M_Item_Original.M_Item_Original");
		ItemMaterialPaths.Emplace("/Game/Materials/StyleDark/M_Item_Dark.M_Item_Dark");
		ItemMaterialPaths.Emplace("/Game/Materials/StyleInverted/M_Item_Inverted.M_Item_Inverted");
		ItemMaterialPaths.Emplace("/Game/Materials/StylePark/M_Item_Park.M_Item_Park");
		MaterialPaths.Emplace(ItemMaterialPaths);

		if (StyleId == static_cast<int32>(ELevelStyle::ELS_Random))
		{
			const auto StyleTypesCount = Settings->GetStylesCount();			
			const auto SelectedStyleId = FMath::RandRange(0, StyleTypesCount - 1);
			StyleId = SelectedStyleId;
		}

		const auto ObstacleMaterialPath = MaterialPaths[0][StyleId];
		const auto EnemyMaterialPath = MaterialPaths[1][StyleId];
		const auto FloorMaterialPath = MaterialPaths[2][StyleId];
		const auto ItemMaterialPath = MaterialPaths[3][StyleId];		

		const auto NoMaterialFoundMessage = "Material can't be found at path: ";
		
		const auto StoredObstacleMaterial = LoadObject<UMaterial>(nullptr, *ObstacleMaterialPath);		
		if (IsValid(StoredObstacleMaterial))
		{
			const auto ProperMaterial = Cast<UMaterial>(StoredObstacleMaterial);
			if (IsValid(ProperMaterial))
			{
				ObstacleMaterial = ProperMaterial;
			}
		}
		else
		{
			ULogger::LogWarning(NoMaterialFoundMessage + ObstacleMaterialPath);
		}
		
		const auto StoredEnemyMaterial = LoadObject<UMaterial>(nullptr, *EnemyMaterialPath);
		if (IsValid(StoredEnemyMaterial))
		{
			const auto ProperMaterial = Cast<UMaterial>(StoredEnemyMaterial);
			if (IsValid(ProperMaterial))
			{
				EnemyMaterial = ProperMaterial;
			}
		}
		else
		{
			ULogger::LogWarning(NoMaterialFoundMessage + EnemyMaterialPath);
		}
		
		const auto StoredFloorMaterial = LoadObject<UMaterial>(nullptr, *FloorMaterialPath);;
		if (IsValid(StoredFloorMaterial))
		{
			const auto ProperMaterial = Cast<UMaterial>(StoredFloorMaterial);
			if (IsValid(ProperMaterial))
			{
				FloorMaterial = ProperMaterial;
			}
		}
		else
		{
			ULogger::LogWarning(NoMaterialFoundMessage + FloorMaterialPath);
		}
		
		const auto StoredItemMaterial = LoadObject<UMaterial>(nullptr, *ItemMaterialPath);
		if (IsValid(StoredItemMaterial))
		{
			const auto ProperMaterial = Cast<UMaterial>(StoredItemMaterial);
			if (IsValid(ProperMaterial))
			{
				ItemMaterial = ProperMaterial;
			}
		}
		else
		{
			ULogger::LogWarning(NoMaterialFoundMessage + ItemMaterialPath);
		}
	}	
}

void ASSGGameMode::SetGameOver(AMainCharacter* PlayerCharacter, const bool& bIsCurrentGameWon)
{
	if (bIsCurrentGameWon)
	{
		bIsGameWon = true;
	}
	else
	{
		bIsGameLost = true;
	}

	if (!IsValid(PlayerCharacter))
	{
		ULogger::LogError("SetGameOver. Player character can't be found or is corrupted! Can't end game!!");
		return;
	}	

	auto PlayerController = Cast<AMainCharacterController>(PlayerCharacter->GetController());
	if (!IsValid(PlayerController))
	{
		ULogger::LogError("SetGameOver. Player controller can't be found or is corrupted! Can't end game!!");
		return;
	}	

	ResetLevelRestartTimer();
	PlayerController->SetIgnoreMoveInput(true);	
	PlayerController->OpenEndGameMenu();	
}

void ASSGGameMode::ResetLevelRestartTimer()
{
	if (GetWorldTimerManager().IsTimerActive(LevelRestartTimer))
	{
		GetWorldTimerManager().ClearTimer(LevelRestartTimer);
	}

	LevelRestartElapsedTimeSec = 0;
	
	GetWorldTimerManager().SetTimer
	(
		LevelRestartTimer, 
		this, 
		&ASSGGameMode::OnLevelRestartTimerTick, 
		1.0F, 
		true, -1.0F
	);
}

void ASSGGameMode::OnLevelRestartTimerTick()
{
	++LevelRestartElapsedTimeSec;

	if (LevelRestartElapsedTimeSec < LevelRestartIntervalSec) return;

	if (GetWorldTimerManager().IsTimerActive(LevelRestartTimer))
	{
		GetWorldTimerManager().ClearTimer(LevelRestartTimer);
	}

	RestartLevel();
}

void ASSGGameMode::RestartLevel() const
{
	const auto World = GetWorld();

	if (!IsValid(World))
	{
		ULogger::LogError("RestartLevel. Can't get world context!");
		return;
	}

	const auto LevelName = UGameplayStatics::GetCurrentLevelName(World);
	UGameplayStatics::OpenLevel(World, FName(LevelName));	
}

bool ASSGGameMode::SpawnAll(const FVector& PlayerLocation)
{	
	ApplyStyle();
	
	auto ObstacleSpawner = GetObstacleSpawnVolume();	
	if (!IsValid(ObstacleSpawner))
	{
		ULogger::LogWarning(
			"SSGGameMode. SpawnAll. Can't locate ObstacleSpawnVolume, level construction continues w/o obstacles");
	}
	else
	{
		ObstacleSpawner->RegisterSpawnActor();
		const auto AreObstaclesSpawnedSuccessfully = ObstacleSpawner->SpawnAllActors(ObstacleMaterial);
		if (!AreObstaclesSpawnedSuccessfully)
		{
			ULogger::LogError(
				"SSGGameMode. SpawnAll. ObstacleSpawnVolume is located, but not all obstacles could be spawn. Restarting.");
			return false;
		}
	}

	auto ItemSpawner = GetItemSpawnVolume();	
	if (!IsValid(ItemSpawner))
	{
		ULogger::LogWarning(
			"SSGGameMode. SpawnAll. Can't locate ItemSpawnVolume, level construction continues w/o items");
	}
	else
	{
		ItemSpawner->RegisterSpawnActor();
		const auto AreItemsSpawnedSuccessfully = ItemSpawner->SpawnAllActors(ItemMaterial);
		if (!AreItemsSpawnedSuccessfully)
		{
			ULogger::LogError(
				"SSGGameMode. SpawnAll. ItemSpawnVolume is located, but not all items could be spawn. Restarting.");
			return false;
		}
	}

	auto EnemySpawner = GetEnemySpawnVolume(PlayerLocation);
	if (!IsValid(EnemySpawner))
	{
		ULogger::LogWarning(
			"SSGGameMode. SpawnAll. Can't locate EnemySpawnVolume, level construction continues w/o enemies");
	}
	else
	{
		EnemySpawner->RegisterSpawnActor();
		const auto AreEnemiesSpawnedSuccessfully = EnemySpawner->SpawnAllActors(EnemyMaterial);
		if (!AreEnemiesSpawnedSuccessfully)
		{
			ULogger::LogError(
				"SSGGameMode. SpawnAll. EnemySpawnVolume is located, but not all items could be spawn. Restarting.");
			return false;
		}
	}

	PaintFloor(FloorMaterial);
	
	return true;
}

AEnemySpawnVolume* ASSGGameMode::GetEnemySpawnVolume(const FVector& PlayerLocation) const
{
	const auto World = GetWorld();

	if (!IsValid(World))
	{
		ULogger::LogError("SSGGameMode. GetEnemySpawnVolume. Can't find world context! SpawnVolumes are inaccessible!!");
		return nullptr;
	}	
	
	for (TActorIterator<AActor> Iterator(World); Iterator; ++Iterator)
	{
		auto EnemySpawnVolume = Cast<AEnemySpawnVolume>(*Iterator);
		if (IsValid(EnemySpawnVolume))
		{			
			EnemySpawnVolume->SetPlayerSpawnLocation(PlayerLocation);
			return EnemySpawnVolume;
		}		
	}

	return nullptr;
}

AObstacleSpawnVolume* ASSGGameMode::GetObstacleSpawnVolume() const
{
	const auto World = GetWorld();

	if (!IsValid(World))
	{
		ULogger::LogError("SSGGameMode. GetObstacleSpawnVolume. Can't find world context! SpawnVolumes are inaccessible!!");
		return nullptr;
	}

	for (TActorIterator<AActor> Iterator(World); Iterator; ++Iterator)
	{
		auto ObstacleSpawnVolume = Cast<AObstacleSpawnVolume>(*Iterator);
		if (IsValid(ObstacleSpawnVolume))
		{
			return ObstacleSpawnVolume;
		}
	}

	return nullptr;
}

AItemSpawnVolume* ASSGGameMode::GetItemSpawnVolume() const
{
	const auto World = GetWorld();

	if (!IsValid(World))
	{
		ULogger::LogError("SSGGameMode. GetItemSpawnVolume. Can't find world context! SpawnVolumes are inaccessible!!");
		return nullptr;
	}

	for (TActorIterator<AActor> Iterator(World); Iterator; ++Iterator)
	{
		auto ItemSpawnVolume = Cast<AItemSpawnVolume>(*Iterator);
		if (IsValid(ItemSpawnVolume))
		{
			return ItemSpawnVolume;
		}
	}	
	
	return nullptr;
}

void ASSGGameMode::PaintFloor(UMaterialInterface* Material) const
{
	const auto World = GetWorld();

	if (!IsValid(World))
	{
		ULogger::LogWarning("SSGGameMode. Paint Floor. Can't find world context! Floor stays with default material!!");
		return;
	}
	
	for (TActorIterator<AActor> Iterator(GetWorld()); Iterator; ++Iterator) {
		if (Iterator->GetName().Contains("Floor_"))
		{
			TArray<UStaticMeshComponent*> MaterialComponents;
			Iterator->GetComponents(MaterialComponents);

			for (int j = 0; j < MaterialComponents.Num(); j++)
			{
				auto TargetComponent = MaterialComponents[j];
				auto MaterialsCount = TargetComponent->GetNumMaterials();

				for (int k = 0; k < MaterialsCount; k++)
				{
					TargetComponent->SetMaterial(k, Material);
				}
			}
		}
	}
}
