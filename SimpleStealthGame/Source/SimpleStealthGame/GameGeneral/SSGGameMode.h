// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "SSGGameMode.generated.h"

class AMainCharacter;
/*
 * Styles of main map appearance
 */
UENUM(BlueprintType)
enum class ELevelStyle : uint8
{
	ELS_Original UMETA(DisplayName = "Original"),
	ELS_Dark UMETA(DisplayName = "Dark"),
	ELS_Inverted UMETA(DisplayName = "Inverted"),
	ELS_Park UMETA(DisplayName = "Park"),
	ELS_Random UMETA(DisplayName = "Random"),
	ELS_MAX UMETA(DisplayName = "MAX")
};

/**
 * Game mode: settings, win-lose logic, etc
 */
UCLASS()
class SIMPLESTEALTHGAME_API ASSGGameMode : public AGameMode
{
	GENERATED_BODY()

private:
	bool bIsGameWon;

	bool bIsGameLost;
	
	FString GameWonText;
	
	FString GameLostText;

	/* Timer that handles waiting for level to restart if no end game option selected */
	FTimerHandle LevelRestartTimer;

	/* Seconds to wait until forced level restart */
	uint8 LevelRestartIntervalSec;

	/* Seconds count by which level restart interval would be incremented in end game menu */
	uint8 LevelRestartIntervalIncrementSec;

	/* Seconds ticked while waiting for level restart */
	uint8 LevelRestartElapsedTimeSec;

	/* Materials to be used by actors on level (material sets reflect the selected style) */
	UPROPERTY()
	class UMaterial* ObstacleMaterial;
	UPROPERTY()
	UMaterial* EnemyMaterial;
	UPROPERTY()
	UMaterial* ItemMaterial;
	UPROPERTY()
	UMaterial* FloorMaterial;	
	
public:
	ASSGGameMode();	

	/* Sets game over - win or lose */
	void SetGameOver(class AMainCharacter* Character, const bool& bIsCurrentGameWon);	

protected:
	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool IsGameWon() const { return bIsGameWon; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool IsGameLost() const { return bIsGameLost; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE FString GetGameWonText() const { return GameWonText; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE FString GetGameLostText() const { return GameLostText; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE int32 GetRemainingTimeForLevelRestart() // !! don't make const !!
	{
		return LevelRestartIntervalSec - LevelRestartElapsedTimeSec;
	}

	/* Spawn all obstacles, items and enemies on level */
	UFUNCTION(BlueprintCallable)
	bool SpawnAll(const FVector& PlayerLocation);	
	
	void RestartLevel() const;

private:
	void ResetLevelRestartTimer();
	
	void OnLevelRestartTimerTick();

	class AObstacleSpawnVolume* GetObstacleSpawnVolume() const;

	class AItemSpawnVolume* GetItemSpawnVolume() const;
	
	class AEnemySpawnVolume* GetEnemySpawnVolume(const FVector& PlayerLocation) const;

	/* Applies the style from settings and sets the materials accordingly */
	void ApplyStyle();

	/* Applies the style from settings to the level floor */
	void PaintFloor(class UMaterialInterface* Material) const;
};
