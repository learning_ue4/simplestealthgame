// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Obstacle.generated.h"

/*
 * An obstacle for player and NPCs that can be spawned via special spawner
 * or places on the scene manually
 */
UCLASS()
class SIMPLESTEALTHGAME_API AObstacle : public AActor
{
	GENERATED_BODY()

private:
	/* A mesh of an actual obstacle */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;

public:
	// Sets default values for this actor's properties
	AObstacle();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/* Initialize default parameters */
	void InitDefaults();
};
