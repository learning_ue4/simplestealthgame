// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SimpleStealthGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESTEALTHGAME_API ASimpleStealthGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
