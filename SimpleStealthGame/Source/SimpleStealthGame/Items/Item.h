// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"

/*
 * An item that can be carried, thrown or moved by the player or a NPC 
 */
UCLASS()
class SIMPLESTEALTHGAME_API AItem : public AActor
{
	GENERATED_BODY()

private:
	/* A mesh of an actual item */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;

	/* Sphere, in which the item is available for interaction */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Interaction, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* InteractionSphere;	

	/* Initial location of the item when it's spawned or placed manually */
	FVector InitialLocation;

	/* Is an item currently being carried by the player or a NPC ? */
	bool bIsBeingCarried;

	/* Item is set as a target for some enemy*/
	bool bIsATarget;

	/* Noise that item makes when thrown -  range in meters */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Interaction, meta = (AllowPrivateAccess = "true"))
	float NoiseRangeMeters;

	/* For calculation purposes only */
	float NoiseRangeCm;

	/* Loudness of noise that item makes when thrown - 0 to 1*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Interaction, meta = (AllowPrivateAccess = "true"))
	float NoiseLoudness;

	/* Who carried the item before it was dropped/thrown */
	class AGameCharacter* PreviousCarrier;

	/* Noise emitter for pawn to be sensed by AI */
	class UPawnNoiseEmitterComponent* NoiseEmitter;

	/*
	 * Timer to check if an item should enable physics.
	 * If item isn't moving and isn't carried and is on the ground,
	 * we should disable physics, because otherwise bots will start
	 * playing football and turn our game into a complete mess
	 */
	FTimerHandle CheckPhysicsStateTimer;

	/* Interval to check if physics should be enabled so we don't have to use Tick() function */
	float CheckPhysicsTimerIntervalSec;

public:
	// Sets default values for this actor's properties
	AItem();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* When the item is dropped - notify hit for enemy's senses */
	UFUNCTION()
	virtual void NotifyHit(UPrimitiveComponent* MyComp,
	                       AActor* Other,
	                       UPrimitiveComponent* OtherComp,
	                       bool bSelfMoved,
	                       FVector HitLocation,
	                       FVector HitNormal,
	                       FVector NormalImpulse,
	                       const FHitResult& hit) override;

	/* Events occured on start colliding (overlapping) with item */
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                    AActor* OtherActor,
	                    UPrimitiveComponent* OtherComp,
	                    int32 OtherBodyIndex,
	                    bool bFromSweep,
	                    const FHitResult& SweepResult);

	/* Events occured on end colliding (overlapping) with item */
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                  AActor* OtherActor,
	                  UPrimitiveComponent* OtherComp,
	                  int32 OtherBodyIndex);

	/* Gets item's initial location - location it was spawned at */
	FORCEINLINE const FVector& GetInitialLocation() const { return InitialLocation; }

	/* Set initial location - used when spawning from external places (i.e. spawn volumes) */
	FORCEINLINE void SetInitialLocation(const FVector& LocationToSet) { InitialLocation = LocationToSet; }

	/* Is item moved to new location? */
	bool IsDisplaced() const;

	FORCEINLINE const bool& IsBeingCarried() const { return bIsBeingCarried; }

	/* Is carried by checking character */
	bool IsCarriedByMe(class AGameCharacter* CharacterToCheck) const;

	/* Equip item by a character */
	void Equip(class AGameCharacter* CarryingCharacter);

	/* Drop item by carrying character */
	void Drop(AGameCharacter* CarryingCharacter);

	/* Throw item by carrying character */
	void Throw(AGameCharacter* CarryingCharacter, const float& ForceValue);

	/* Set if the item is some enemy's target */
	FORCEINLINE void SetAsTarget(const bool& bIsCurrentlyATarget) { bIsATarget = bIsCurrentlyATarget; };

	/* Is this item currently some enemy's target? */
	FORCEINLINE const bool& IsATarget() const { return bIsATarget; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
private:
	/* Initialize default parameters */
	void InitDefaults();

	/* Initialize interaction parameters */
	void InitInteraction();

	/* Remember the initial location of the item */
	void SetInitialLocation();

	/* Unequip item from carrying character */
	void Unequip(AGameCharacter* CarryingCharacter);

	bool IsMoving() const;

	/* Is item on the ground? Goal is the same as for IsMoving() */
	bool IsOnTheGround() const;

	/* An impulse needed to throw an item at a particular distance (rough calculation for prototype purposes) */
	FVector CalculateThrowingImpulse(const FVector& ViewPoint, const float& Distance) const;

	/* Tick for physics checking timer */
	void OnCheckPhysicsTimerTick() const;

	/* Reset physics checking timer */
	void ResetCheckPhysicsTimer();
};
