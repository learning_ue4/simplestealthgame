// Fill out your copyright notice in the Description page of Project Settings.

#include "Item.h"
#include "Components/SphereComponent.h"
#include "SimpleStealthGame/Common/GameCharacter.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"
#include "Misc/MapErrors.h"
#include "Engine/StaticMeshSocket.h"
#include "SimpleStealthGame/Player/MainCharacter.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Components/PawnNoiseEmitterComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	InitDefaults();
}

void AItem::InitDefaults()
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemMesh"));
	MeshComponent->SetCanEverAffectNavigation(false);
	MeshComponent->SetNotifyRigidBodyCollision(true);
	RootComponent = MeshComponent;	

	InteractionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("ItemCollisionSphere"));
	InteractionSphere->SetupAttachment(RootComponent);
	
	NoiseEmitter = CreateDefaultSubobject<UPawnNoiseEmitterComponent>(TEXT("NoiseEmitter"));
	NoiseRangeMeters = 20.0F;
	NoiseLoudness = 1.0F;

	bIsBeingCarried = false;

	CheckPhysicsTimerIntervalSec = .2F;
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();

	SetInitialLocation();
	InitInteraction();
	ResetCheckPhysicsTimer();
}

void AItem::InitInteraction()
{
	NoiseRangeCm = NoiseRangeMeters * 100;

	InteractionSphere->OnComponentBeginOverlap.AddDynamic(this, &AItem::OnOverlapBegin);
	InteractionSphere->OnComponentEndOverlap.AddDynamic(this, &AItem::OnOverlapEnd);
}

void AItem::SetInitialLocation()
{
	InitialLocation = GetActorLocation();	
}

// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AItem::IsDisplaced() const
{
	const auto CurrentLocation = GetActorLocation();
	return InitialLocation != CurrentLocation;
}

void AItem::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                           AActor* OtherActor,
                           UPrimitiveComponent* OtherComp,
                           int32 OtherBodyIndex,
                           bool bFromSweep,
                           const FHitResult& SweepResult)
{
	if (bIsBeingCarried) return;
	if (!IsValid(OtherActor)) return;

	auto OverlappingCharacter = Cast<AGameCharacter>(OtherActor);

	if (!IsValid(OverlappingCharacter)) return;
	if (OverlappingCharacter->HasActiveOverlappingItem()) return;

	OverlappingCharacter->SetActiveOverlappingItem(this);
}

void AItem::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                         AActor* OtherActor,
                         UPrimitiveComponent* OtherComp,
                         int32 OtherBodyIndex)
{
	if (bIsBeingCarried) return;	
	if (!IsValid(OtherActor)) return;

	auto OverlappingCharacter = Cast<AGameCharacter>(OtherActor);

	if (!IsValid(OverlappingCharacter)) return;
	if (OverlappingCharacter->HasActiveOverlappingItem()) return;

	OverlappingCharacter->SetActiveOverlappingItem(nullptr);
}

void AItem::NotifyHit(UPrimitiveComponent* MyComp,
                      AActor* Other,
                      UPrimitiveComponent* OtherComp,
                      bool bSelfMoved,
                      FVector HitLocation,
                      FVector HitNormal,
                      FVector NormalImpulse,
                      const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	const auto ItemName = UKismetSystemLibrary::GetDisplayName(this);
	const auto Tag = FName("ThrownItemHitSound:" + ItemName);

	const auto CurrentLoudness = FMath::Clamp(NoiseLoudness, .0F, 1.0F);
	APawn* PreviousCarrierPawn = nullptr;

	if (IsValid(PreviousCarrier))
	{
		auto CarryingPlayer = Cast<AMainCharacter>(PreviousCarrier);

		if (IsValid(CarryingPlayer))
		{
			PreviousCarrierPawn = Cast<APawn>(PreviousCarrier);
			MakeNoise(CurrentLoudness, PreviousCarrierPawn, GetActorLocation(), NoiseRangeCm, Tag);
		}
	}
}

void AItem::Equip(class AGameCharacter* CarryingCharacter)
{
	if (!IsValid(CarryingCharacter)) return;

	const auto ItemSocketName = CarryingCharacter->GetItemSocketName();
	
	const UStaticMeshSocket* ItemSocket = CarryingCharacter->GetStaticMesh()->GetSocketByName(*ItemSocketName);

	if (!IsValid(ItemSocket))
	{
		ULogger::LogWarning("Item. Equip. Can't find the desired equip socket. Please check socket names of a character's skeletal mesh.");
		return;
	}

	MeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	MeshComponent->SetSimulatePhysics(false);

	ItemSocket->AttachActor(this, CarryingCharacter->GetStaticMesh());
	CarryingCharacter->SetActiveOverlappingItem(nullptr);
	CarryingCharacter->SetCarriedItem(this);
	
	bIsBeingCarried = true;	
}

void AItem::Unequip(AGameCharacter* CarryingCharacter)
{
	if (!bIsBeingCarried) return;
	
	if (!IsValid(CarryingCharacter)) return;

	const auto ItemSocketName = CarryingCharacter->GetItemSocketName();

	const UStaticMeshSocket* ItemSocket = CarryingCharacter->GetStaticMesh()->GetSocketByName(*ItemSocketName);

	if (!IsValid(ItemSocket))
	{
		ULogger::LogWarning("Item. Unequip. Can't find the desired equip socket. Please check socket names of a skeletal mesh");
		return;
	}

	PreviousCarrier = CarryingCharacter;
	CarryingCharacter->SetCarriedItem(nullptr);

	RootComponent->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);	
	
	MeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Block);
	MeshComponent->SetSimulatePhysics(true);

	bIsBeingCarried = false;
}

void AItem::Throw(AGameCharacter* CarryingCharacter, const float& Distance)
{
	Unequip(CarryingCharacter);
	
	if (bIsBeingCarried) return;

	MeshComponent->SetSimulatePhysics(true);

	// Rough calculation of throwing distance for prototyping purposes
	const auto CarryingCharacterController = CarryingCharacter->Controller;

	if (!IsValid(CarryingCharacterController)) return;

	FVector CarriersLocation;
	FRotator CarriersRotation;
	CarryingCharacterController->GetPlayerViewPoint(CarriersLocation, CarriersRotation);
	const auto ViewPoint = FVector(CarriersRotation.Vector().GetSafeNormal());
	const auto Impulse = CalculateThrowingImpulse(ViewPoint, Distance * 100);
	
	MeshComponent->AddImpulse(Impulse, NAME_None, true);
}

void AItem::Drop(AGameCharacter* CarryingCharacter)
{
	Unequip(CarryingCharacter);
}

bool AItem::IsCarriedByMe(AGameCharacter* CharacterToCheck) const
{
	return bIsBeingCarried && PreviousCarrier == CharacterToCheck;
}

FVector AItem::CalculateThrowingImpulse(const FVector& ViewPoint, const float& Distance) const
{
	auto ResultImpulse = FVector::ZeroVector;
	const auto Location = GetActorLocation();
	const auto TargetLocation = FVector(Location + (ViewPoint * Distance));

	UGameplayStatics::SuggestProjectileVelocity_CustomArc(GetWorld(), ResultImpulse, Location, TargetLocation, 0, .7F);

	return ResultImpulse;	
}

void AItem::ResetCheckPhysicsTimer()
{
	GetWorldTimerManager().SetTimer
	(
		CheckPhysicsStateTimer, 
		this, 
		&AItem::OnCheckPhysicsTimerTick, 
		CheckPhysicsTimerIntervalSec, 
		true
	);
}

void AItem::OnCheckPhysicsTimerTick() const
{
	if (!IsMoving() && !bIsBeingCarried && IsOnTheGround())
	{
		MeshComponent->SetSimulatePhysics(false);
	}
}

bool AItem::IsMoving() const
{
	const auto MovementSpeed = FVector::DotProduct(GetVelocity(), GetActorRotation().Vector());
	const auto bIsPhysicallyMoving = !FMath::IsNearlyZero(MovementSpeed);

	return bIsPhysicallyMoving;
}

bool AItem::IsOnTheGround() const
{
	TArray<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors);

	if (OverlappingActors.Num() < 1) return false; /* item is in air so it can't be on the ground */

	for (const auto& OverlappingActor : OverlappingActors)
	{
		const auto OverlappingCharacter = Cast<AGameCharacter>(OverlappingActor);

		if (IsValid(OverlappingCharacter)) return false;
	}

	return true; /* item is not overlapping with any characters, so consider it on the ground */
}
