// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameCharacter.generated.h"

/*
 * General game character - player, AI-driving NPCs, etc
 */
UCLASS(Abstract)
class SIMPLESTEALTHGAME_API AGameCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	/* Base mesh component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = StaticMesh, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh;

	/* Item that is currently overlapped by the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	class AItem* ActiveOverlappingItem;

	/* Item that is currently carried by the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	AItem* CarriedItem;	

private:
	/* Does character have an overlapping item? */
	bool bHasActiveOverlappingItem;

	/* Is character carrying an item right now? */
	bool bHasCarriedItem;

	/* A name of socket on character's mesh, that carried item is attached to */
	FString ItemSocketName;	
	
public:
	// Sets default values for this character's properties
	AGameCharacter();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/* Set character's active overlapping item and corresponding status */
	FORCEINLINE void SetActiveOverlappingItem(AItem* Item)
	{
		ActiveOverlappingItem = Item;
		bHasActiveOverlappingItem = (Item != nullptr);
	}

	/* Set character's currently carrying item */
	FORCEINLINE void SetCarriedItem(AItem* ItemToCarry)
	{
		CarriedItem = ItemToCarry;
		bHasCarriedItem = (ItemToCarry != nullptr);
	}
	
	FORCEINLINE const FString& GetItemSocketName() const { return ItemSocketName; }
	FORCEINLINE const bool& HasActiveOverlappingItem() const { return bHasActiveOverlappingItem; }
	FORCEINLINE const bool& HasCarriedItem() const { return bHasCarriedItem; }
	FORCEINLINE class UStaticMeshComponent* GetStaticMesh() const { return StaticMesh; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	/* Pick surrounding item if needed */
	void PickupItem();

	/* Pass away */
	virtual void Die();
};
