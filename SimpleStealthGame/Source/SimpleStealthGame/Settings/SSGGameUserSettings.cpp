// Fill out your copyright notice in the Description page of Project Settings.


#include "SSGGameUserSettings.h"

USSGGameUserSettings::USSGGameUserSettings()
{
	SetDefaults();
}

void USSGGameUserSettings::SetDefaults()
{
	GameSpeedRatio = 1.F;
	GameSpeedRatioMinValue = .25F;
	GameSpeedRatioMaxValue = 2.F;
	BaseGameSpeed = 500;
	StyleId = 0;
	StylesCount = 4;
}

void USSGGameUserSettings::ApplySettings(bool bCheckForCommandLineOverrides)
{
	Super::ApplySettings(bCheckForCommandLineOverrides);
}

float USSGGameUserSettings::GetGameSpeedRatio() const
{
	return GameSpeedRatio;
}

float USSGGameUserSettings::GetGameSpeedRatioMinValue() const
{
	return GameSpeedRatioMinValue;
}

float USSGGameUserSettings::GetGameSpeedRatioMaxValue() const
{
	return GameSpeedRatioMaxValue;
}

void USSGGameUserSettings::SetGameSpeedRatio(const float& GameSpeedRatioToSet)
{
	GameSpeedRatio = GameSpeedRatioToSet;
}

void USSGGameUserSettings::SetStyleId(const int32& StyleIdToSet)
{
	StyleId = StyleIdToSet % (StylesCount + 1);
}

float USSGGameUserSettings::GetBaseGameSpeed() const
{
	return BaseGameSpeed;
}

int32 USSGGameUserSettings::GetStyleId() const
{
	return StyleId;
}

int8 USSGGameUserSettings::GetStylesCount() const
{
	return StylesCount;
}
