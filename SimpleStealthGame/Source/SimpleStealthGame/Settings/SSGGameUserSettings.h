// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameUserSettings.h"
#include "SSGGameUserSettings.generated.h"

/**
 * An extension for the Settings class.
 * Contains specific game settings.
 */
UCLASS(Blueprintable)
class SIMPLESTEALTHGAME_API USSGGameUserSettings : public UGameUserSettings
{
	GENERATED_BODY()

protected:
	/* Overall game speed multiplier */
	UPROPERTY(config)
	float GameSpeedRatio;

	/* Overall game speed multiplier min possible value */
	UPROPERTY(config)
	float GameSpeedRatioMinValue;

	/* Overall game speed multiplier max possible value */
	UPROPERTY(config)
	float GameSpeedRatioMaxValue;

	/* ID of main level's style */
	UPROPERTY(config)
	int32 StyleId;

private:
	/* Speed at which character moves - base speed for all enemy movements */
	float BaseGameSpeed;

	/* Number of styles available in settings */
	int8 StylesCount;

public:
	USSGGameUserSettings();

	virtual void ApplySettings(bool bCheckForCommandLineOverrides) override;

	/* Sets the overall game speed multiplier */
	UFUNCTION(BlueprintCallable)
		void SetGameSpeedRatio(const float& GameSpeedRatioToSet);

	/* Returns the overall game speed multiplier */
	UFUNCTION(BlueprintCallable)
		float GetGameSpeedRatio() const;

	/* Returns game speed multiplier min possible value */
	UFUNCTION(BlueprintCallable)
		float GetGameSpeedRatioMinValue() const;

	/* Returns game speed multiplier max possible value */
	UFUNCTION(BlueprintCallable)
		float GetGameSpeedRatioMaxValue() const;

	/* Returns base game speed that will be multiplied */
	float GetBaseGameSpeed() const;

	/* Sets main level's style */
	UFUNCTION(BlueprintCallable)
	void SetStyleId(const int32& StyleIdToSet);

	/* Returns ID of a style to be applied to the main level appearance */
	UFUNCTION(BlueprintCallable)
	int32 GetStyleId() const;

	/* Returns the number of styles available */
	int8 GetStylesCount() const;

private:
	/* Set default values */
	void SetDefaults();
};
