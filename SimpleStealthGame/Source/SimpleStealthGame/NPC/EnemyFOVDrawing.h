﻿#pragma once

#include "EnemyFOVDrawing.generated.h"

/*
 * A structure with data for drawing the debug cone
 * of enemy's sight spline
 */
USTRUCT(BlueprintType)
struct FEnemyFOVDrawing
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = FOVDrawing)
	FLinearColor Color;

	UPROPERTY(VisibleAnywhere, Category = FOVDrawing)
	int32 Thickness;

	UPROPERTY(VisibleAnywhere, Category = FOVDrawing)
	int32 SidesCount;

	/* a divider for angle height drawing a debug cone */
	UPROPERTY(VisibleAnywhere, Category = FOVDrawing)
	float AngleHeightCoef; 
};
