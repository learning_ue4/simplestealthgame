 // Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "EngineUtils.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Actor.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISenseConfig.h"
#include "SimpleStealthGame/Player/MainCharacter.h"
#include "SimpleStealthGame/Items/Item.h"
#include "SimpleStealthGame/NPC/EnemyAIController.h"
#include "DrawDebugHelpers.h"
#include "TimerManager.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"
#include "SimpleStealthGame/Settings/SSGGameUserSettings.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/Engine.h"

// Sets default values
AEnemy::AEnemy()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InitDefaults();
}

void AEnemy::InitDefaults()
{	
	ChasingPlayerSpeedRatio = 2.0F;
	ReturningToSpawnLocationSpeedRatio = .2F;
	ItemSearchingMovementSpeedRatio = .5F;
	CarryingItemSpeedRatio = .5F;
	LookAroundRotationRate = 90;
	CurrentLookAroundValue = .0F;

	MinLookAroundIntervalSec = 3;
	MaxLookAroundIntervalSec = 12;
	CheckLastSeenPlayerLocationTimeSec = 5.0F;
	ChasePlayerIntervalSec = .2F;
	ReturnIntervalSec = .2F;
	ItemSearchingTimerIntervalSec = .2F;
	CarryingItemIntervalSec = .2F;

	FOVDrawing.Color = FLinearColor::Blue;
	FOVDrawing.Thickness = 1.0F;
	FOVDrawing.SidesCount = 16.0F;
	FOVDrawing.AngleHeightCoef = 10.0F;

	SightRayCastDistanceMeters = 20.0F;
	SightFOVDegrees = 70.0F;
	SightMaxAgeSeconds = 10.0F;
	HearingRangeMeters = 20.0F;
	HearingMaxAgeSeconds = 10.0F;	

	TargetPlayer = nullptr;
	TargetItem = nullptr;
	AIState = EEnemyAIState::EMS_Idle;

	InitPerception();
	SetRandomViewDirection();
	NewRotationYaw = .0F;
}

void AEnemy::InitPerception()
{
	SightRayCastDistanceCm = SightRayCastDistanceMeters * 100; /* for calculation purposes */

	PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerceptionComponent"));	

	Sight = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("AISight"));

	Sight->SightRadius = SightRayCastDistanceCm;
	Sight->PeripheralVisionAngleDegrees = SightFOVDegrees;
	Sight->LoseSightRadius = SightRayCastDistanceCm;
	Sight->DetectionByAffiliation.bDetectNeutrals = true;
	Sight->DetectionByAffiliation.bDetectEnemies = true;
	Sight->DetectionByAffiliation.bDetectFriendlies = true;	
	Sight->AutoSuccessRangeFromLastSeenLocation = -1;	
	Sight->SetMaxAge(SightMaxAgeSeconds);
	PerceptionComponent->ConfigureSense(*Sight);

	Hearing = CreateDefaultSubobject<UAISenseConfig_Hearing>(TEXT("AIHearing"));

	const auto HearingRadius = HearingRangeMeters * 100;
	Hearing->HearingRange = HearingRadius;
	Hearing->bUseLoSHearing = true;
	Hearing->LoSHearingRange = HearingRadius;
	Hearing->DetectionByAffiliation.bDetectNeutrals = true;
	Hearing->DetectionByAffiliation.bDetectEnemies = true;
	Hearing->DetectionByAffiliation.bDetectFriendlies = true;
	Hearing->SetMaxAge(HearingMaxAgeSeconds);
	PerceptionComponent->ConfigureSense(*Hearing);

	PerceptionComponent->SetDominantSense(Sight->GetSenseImplementation());
}

void AEnemy::SetRandomViewDirection()
{
	auto Rotation = GetActorRotation();
	Rotation.Yaw = FMath::FRand() * 360;
	SetActorRotation(Rotation);
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();	
	
	CalculateSpeed();
	InitAIController();
	SetIdleState();	
	InitSubscriptions();
}

void AEnemy::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	ClearActiveTimers();
}

void AEnemy::CalculateSpeed()
{
	auto PlayerSpeed = .0F;
	
	if (IsValid(GEngine))
	{
		const auto GameSettings = Cast<USSGGameUserSettings>(GEngine->GetGameUserSettings());

		if (!IsValid(GameSettings))
		{
			ULogger::LogError("Game user settings are not initialized! All movements are impossible!");
			return;
		}

		PlayerSpeed = GameSettings->GetBaseGameSpeed() * GameSettings->GetGameSpeedRatio();
	}
	
	ChasingPlayerSpeed = PlayerSpeed * ChasingPlayerSpeedRatio;
	ReturningToSpawnLocationSpeed = ChasingPlayerSpeed * ReturningToSpawnLocationSpeedRatio;
	ItemSearchingMovementSpeed = ChasingPlayerSpeed * ItemSearchingMovementSpeedRatio;
	CarryingItemSpeed = ChasingPlayerSpeed * CarryingItemSpeedRatio;	
}

void AEnemy::InitAIController()
{
	AIController = Cast<AEnemyAIController>(GetController());
	SpawnLocation = GetActorLocation();
	/* at the beginning of the game enemy doesn't know about the player so his
	 "last seen player" location is his spawn location */
	TargetPlayerLastSeenLocation = SpawnLocation;
	SetCurrentRotation(GetActorRotation());
}

void AEnemy::SetCurrentRotation(const FRotator& RotationToSet)
{
	CurrentRotation = RotationToSet;
}

void AEnemy::SetIdleState()
{
	ClearActiveTimers();
	AIState = EEnemyAIState::EMS_Idle;
	ResetIdleStateTimer();
}

void AEnemy::ClearActiveTimers()
{
	GetWorldTimerManager().ClearAllTimersForObject(this);
	GetWorldTimerManager().ClearAllTimersForObject(this);
}

void AEnemy::ResetIdleStateTimer()
{
	const auto TimeOut = FMath::FRandRange(MinLookAroundIntervalSec, MaxLookAroundIntervalSec);
	GetWorldTimerManager().SetTimer(IdleStateTimer, this, &AEnemy::OnIdleStateTimerTick, TimeOut, true);
	CurrentLookAroundValue = .0F;
}

void AEnemy::OnIdleStateTimerTick()
{
	if (AIState != EEnemyAIState::EMS_Idle && AIState != EEnemyAIState::EMS_LookingAround) return;

	NewRotationYaw = FMath::FRandRange(-180.0F, 180.0F);
	AIState = EEnemyAIState::EMS_LookingAround;
}

void AEnemy::InitSubscriptions()
{
	PerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemy::OnTargetPerceptionUpdated);
	PerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AEnemy::OnPerceptionUpdated);
}

void AEnemy::OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	if (!IsValid(Actor)) return;
	if (!Stimulus.IsValid() || !Stimulus.WasSuccessfullySensed()) return;

	if (IsSightStimulus(Stimulus))
	{
		const auto TargetPlayerCharacter = Cast<AMainCharacter>(Actor);

		if (!IsValid(TargetPlayerCharacter)) return;
		
		if (TargetPlayerCharacter->IsDead()) return;

		if (TargetPlayer == TargetPlayerCharacter) return;

		SetTargetCharacter(TargetPlayerCharacter);
		
	}
	else if (IsHearingStimulus(Stimulus))
	{				
		if (IsTargetPlayerValid()) return; /* ignore hearing when chasing the player */

		auto* World = GetWorld();

		if (!IsValid(World)) return;

		AItem* Item = nullptr;
		const auto ItemName = GetStimulatingItemName(Stimulus);

		for (TActorIterator<AActor> Iterator(World); Iterator; ++Iterator)
		{
			const auto IteratedItem = Cast<AItem>(*Iterator);

			if (!IsValid(IteratedItem)) continue;

			auto IteratedItemName = UKismetSystemLibrary::GetDisplayName(IteratedItem);

			if (ItemName == IteratedItemName)
			{
				Item = IteratedItem;
				break;
			}			
		}

		SetTargetItem(Item);		
	}
}

bool AEnemy::IsTargetPlayerValid() const
{
	return (IsValid(TargetPlayer) && !TargetPlayer->IsDead());
}

void AEnemy::SetTargetItem(AItem* TargetItemToSet)
{
	if (TargetItemToSet == TargetItem) return;
	if (IsValid(TargetItemToSet) && (TargetItemToSet->IsBeingCarried() || TargetItemToSet->IsATarget())) return;
	
	if (AIState == EEnemyAIState::EMS_CarryingItem)
	{
		if (!IsValid(TargetItem)) return;
		
		TargetItem->Drop(this);
		TargetItem->SetAsTarget(false);
	}
	
	TargetItem = TargetItemToSet;
	if (IsValid(TargetItemToSet))
	{
		TargetItemToSet->SetAsTarget(true);
	}	

	if (IsValid(TargetItem))
	{
		TargetItemLastHeardLocation = TargetItem->GetActorLocation();

		if (AIState != EEnemyAIState::EMS_ChasingPlayer)
		{
			SetItemSearchingState();
		}
	}	
}

void AEnemy::SetItemSearchingState()
{
	if (AIState == EEnemyAIState::EMS_ItemSearching) return;	

	AIState = EEnemyAIState::EMS_ItemSearching;	

	ClearActiveTimers();
	GetCharacterMovement()->MaxWalkSpeed =ItemSearchingMovementSpeed;
	ResetItemSearchingTimer();
}

void AEnemy::ResetItemSearchingTimer()
{
	GetWorldTimerManager().SetTimer
	(
		ItemSearchingTimer,
		this,
		&AEnemy::OnItemSearchingTimerTick,
		ItemSearchingTimerIntervalSec,
		true,
		-1.0F
	);
}

void AEnemy::OnItemSearchingTimerTick()
{
	if (AIState != EEnemyAIState::EMS_ItemSearching) return;

	if (TargetItem && !TargetItem->IsBeingCarried())
	{
		TargetItemLastHeardLocation = TargetItem->GetActorLocation();
	}
	else
	{
		SetTargetItem(nullptr);
	}

	if (!IsValid(AIController)) return;

	const auto MoveResult = AIController->MoveToLocation(TargetItemLastHeardLocation, -1.0F);
	const auto Distance = FVector::Distance(GetActorLocation(), TargetItemLastHeardLocation);

	if (MoveResult == EPathFollowingRequestResult::AlreadyAtGoal || Distance <= 100)
	{
		AIController->StopMovement();
		SetRandomViewDirection();
		SetCheckLastSeenPlayerLocationState();
	}
}

FString AEnemy::GetStimulatingItemName(const FAIStimulus& Stimulus) const
{
	auto Result = FString();
	
	const auto Index = Stimulus.Tag.ToString().Find(":");
	if (Index > 0)
	{
		Result = Stimulus.Tag.ToString().RightChop(Index + 1);
	}

	return Result;
}

void AEnemy::OnPerceptionUpdated(const TArray<AActor*>& UpdatedActors)
{
	if (!IsTargetPlayerValid()) return;

	for (const auto& UpdatedActor : UpdatedActors)	
	{
		if (!IsValid(UpdatedActor)) continue;

		const auto UpdatedPlayerCharacter = Cast<AMainCharacter>(UpdatedActor);
		if (!IsValid(UpdatedPlayerCharacter)) continue;

		FActorPerceptionBlueprintInfo PerceptionInfo;
		PerceptionComponent->GetActorsPerception(UpdatedActor, PerceptionInfo);
		if (PerceptionInfo.LastSensedStimuli.Num() < 2) continue; /* sight and hearing */

		/*
		 * checking sight only, because we are not interested in audition of the actors
		 * themselves, only their hits
		 */
		const FAIStimulus& SightStimulus = PerceptionInfo.LastSensedStimuli[0];
		if (!SightStimulus.IsValid() || SightStimulus.IsActive()) continue;

		/*
		 * If target player is detected but sight stimulus isn't active (lost sight of player) -
		 *	reset target character
		 */
		if (TargetPlayer)
		{
			SetTargetCharacter(nullptr);
		}
	}
}

void AEnemy::SetTargetCharacter(AMainCharacter* TargetCharacter)
{
	if (TargetPlayer == TargetCharacter) return;

	TargetPlayer = TargetCharacter; // can be null

	if (IsValid(TargetPlayer))
	{
		if (IsValid(TargetItem))
		{
			SetTargetItem(nullptr);
		}

		TargetPlayerLastSeenLocation = TargetPlayer->GetActorLocation();
		SetChasePlayerState();
	}
}

bool AEnemy::IsSightStimulus(const FAIStimulus& Stimulus) const
{
	return Stimulus.Type.Name.ToString().Contains("Sight");
}

bool AEnemy::IsHearingStimulus(const FAIStimulus& Stimulus) const
{
	return Stimulus.Tag.ToString().Contains("Sound");
}

void AEnemy::SetChasePlayerState()
{
	if (AIState == EEnemyAIState::EMS_ChasingPlayer) return;
	if (!IsTargetPlayerValid()) return;

	AIState = EEnemyAIState::EMS_ChasingPlayer;

	ClearActiveTimers();
	GetCharacterMovement()->MaxWalkSpeed = ChasingPlayerSpeed;
	ResetChasePlayerTimer();
}

void AEnemy::ResetChasePlayerTimer()
{
	GetWorldTimerManager().SetTimer
	(
		ChasePlayerTimer,
		this,
		&AEnemy::OnChasePlayerTimerTick,
		ChasePlayerIntervalSec,
		true
	);
}

void AEnemy::OnChasePlayerTimerTick()
{
	if (AIState != EEnemyAIState::EMS_ChasingPlayer) return;
	if (!IsValid(AIController)) return;

	if (IsTargetPlayerValid())
	{
		TargetPlayerLastSeenLocation = TargetPlayer->GetActorLocation();		
		SetCurrentRotation(TargetPlayerLastSeenLocation.Rotation());
	}	
	
	AIController->MoveToLocation(TargetPlayerLastSeenLocation, 5.0F);
		
	if (!IsTargetPlayerValid())
	{
		AIController->StopMovement();
		SetCheckLastSeenPlayerLocationState();
	}		
}

void AEnemy::SetCheckLastSeenPlayerLocationState()
{
	if (AIState == EEnemyAIState::EMS_CheckingLastSPlayersLocation) return;

	ClearActiveTimers();

	CurrentLookAroundValue = .0F;
	
	AIState = EEnemyAIState::EMS_CheckingLastSPlayersLocation;
	ResetCheckLastSeenPlayerLocationTimer();
}

void AEnemy::ResetCheckLastSeenPlayerLocationTimer()
{
	GetWorldTimerManager().SetTimer
	(
		CheckLastSeenPlayersLocationTimer,
		this,
		&AEnemy::OnCheckLastSeenPlayerLocationTimerTick,
		CheckLastSeenPlayerLocationTimeSec, 
		true
	);
}

void AEnemy::OnCheckLastSeenPlayerLocationTimerTick()
{
	if (AIState != EEnemyAIState::EMS_CheckingLastSPlayersLocation)
	{
		return;
	}

	if (TargetItem && !TargetItem->IsBeingCarried())
	{
		SetCarryingItemState();
	}
	else
	{
		SetReturningState();
	}	
}

void AEnemy::SetCarryingItemState()
{
	if (AIState == EEnemyAIState::EMS_CarryingItem) return;	
	if (!IsValid(TargetItem)) return;

	if (TargetItem->IsBeingCarried() && !TargetItem->IsCarriedByMe(this))
	{
		SetTargetItem(nullptr);
		return;
	}

	ClearActiveTimers();
	TargetItem->Equip(this);

	AIState = EEnemyAIState::EMS_CarryingItem;

	GetCharacterMovement()->MaxWalkSpeed = CarryingItemSpeed;
	ResetCarryingItemTimer();	
}

void AEnemy::ResetCarryingItemTimer()
{
	GetWorldTimerManager().SetTimer
	(
		CarryingItemTimer,
		this,
		&AEnemy::OnCarryingItemTimerTick,
		CarryingItemIntervalSec,
		true,
		-1.0F
	);
}

void AEnemy::OnCarryingItemTimerTick()
{
	if (AIState != EEnemyAIState::EMS_CarryingItem) return;
	if (!IsValid(TargetItem)) return;
		
	const auto MoveResult = AIController->MoveToLocation(TargetItem->GetInitialLocation(), -1.0F);
	const auto Distance = FVector::Distance(GetActorLocation(), TargetItem->GetInitialLocation());

	if (MoveResult == EPathFollowingRequestResult::AlreadyAtGoal || Distance <= 50)
	{
		AIController->StopMovement();		
		SetReturningState();
	}	
}

void AEnemy::SetReturningState()
{	
	if (AIState == EEnemyAIState::EMS_Returning) return;
	if (!IsValid(AIController)) return;

	AIState = EEnemyAIState::EMS_Returning;

	if (IsValid(TargetItem))
	{
		TargetItem->Drop(this);
		TargetItem->SetAsTarget(false);
		SetTargetItem(nullptr);
	}

	ClearActiveTimers();
	GetCharacterMovement()->MaxWalkSpeed = ReturningToSpawnLocationSpeed;
	ResetReturningTimer();
}

void AEnemy::ResetReturningTimer()
{
	GetWorldTimerManager().SetTimer
	(
		ReturningTimer,
		this,
		&AEnemy::OnReturningTimerTick,
		ReturnIntervalSec,
		true
	);
}

void AEnemy::OnReturningTimerTick()
{
	if (AIState != EEnemyAIState::EMS_Returning) return;
	if (!IsValid(AIController)) return;

	SetCurrentRotation(SpawnLocation.Rotation());

	const auto MoveResult = AIController->MoveToLocation(SpawnLocation, -1.0F);
	const auto Distance = FVector::Distance(GetActorLocation(), SpawnLocation);

	if (MoveResult == EPathFollowingRequestResult::AlreadyAtGoal || Distance <= 300)
	{
		AIController->StopMovement();
		SetRandomViewDirection();
		SetIdleState();
	}
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DrawFOVImage();

	if (AIState == EEnemyAIState::EMS_LookingAround)
	{	
		LookAround(DeltaTime);

		/* making sure at least one full circle is completed */
		const auto Difference = FMath::FloorToInt(NewRotationYaw - GetActorRotation().Yaw);		
		if (Difference == 0 && CurrentLookAroundValue >= 360.0F)
		{
			AIState = EEnemyAIState::EMS_Idle;
			ResetIdleStateTimer();
		}		
	}
	else if (AIState == EEnemyAIState::EMS_CheckingLastSPlayersLocation)
	{
		LookAround(DeltaTime);
	}
	else
	{
		CorrectCurrentRotation(DeltaTime);
	}
}

void AEnemy::LookAround(const float& DeltaTime)
{
	const auto DeltaYaw = DeltaTime * LookAroundRotationRate;
	
	auto Rotation = GetActorRotation();
	Rotation.Yaw += DeltaYaw;
	CurrentLookAroundValue += DeltaYaw;
	SetActorRotation(Rotation);
	SetCurrentRotation(Rotation);
}

void AEnemy::DrawFOVImage() const
{
	if (FOVDrawing.AngleHeightCoef == .0F)
	{
		ULogger::LogWarning("Enemy. DrawFOVImage. FOVDrawing.AngleHeightCoef is sef to 0, debug cone can't be drawn!");
		return;
	}
	
	const auto* World = GetWorld();
	const auto Origin = GetPawnViewLocation();
	const auto Direction = GetViewRotation().Vector();
	const auto SightFOVRadians = FMath::DegreesToRadians(SightFOVDegrees);
	const auto AngleHeight = SightFOVRadians / FOVDrawing.AngleHeightCoef;
	
	DrawDebugCone
	(
		World,
		Origin,
		Direction,
		SightRayCastDistanceCm,
		SightFOVRadians,
		AngleHeight,
		FOVDrawing.SidesCount,
		FOVDrawing.Color.ToFColor(true),
		false,
		.0F,
		0,
		FOVDrawing.Thickness
	);	
}

void AEnemy::CorrectCurrentRotation(const float& DeltaSeconds)
{
	if (!IsValid(AIController)) return;
	
	const auto& Rotation = AIController->GetControlRotation();

	if (Rotation.Equals(CurrentRotation)) return;
		
	const auto CorrectedRotation = FMath::RInterpTo(Rotation, CurrentRotation, DeltaSeconds, 40);	
	AIController->SetControlRotation(CorrectedRotation);	
}
