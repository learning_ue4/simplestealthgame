// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GameFramework/Pawn.h"
#include "EnemyAIController.generated.h"

/*
 * Enemy's AI controller
 */
UCLASS()
class SIMPLESTEALTHGAME_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()

public:
	AEnemyAIController();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

protected:
	virtual void OnPossess(APawn* PawnToPossess) override;
};
