 // Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "Enemy.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"

 AEnemyAIController::AEnemyAIController()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AEnemyAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	
}

void AEnemyAIController::OnPossess(APawn* PawnToPossess)
{
	Super::OnPossess(PawnToPossess);

	const auto Enemy = Cast<AEnemy>(PawnToPossess);

 	if (!IsValid(Enemy))
 	{
		ULogger::LogError("EnemyAIController. OnPossess. Enemy character to possess not found!");
		return;
 	} 	
}
