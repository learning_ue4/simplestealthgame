// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SimpleStealthGame/Common/GameCharacter.h"
#include "Perception/AIPerceptionTypes.h"
#include "EnemyFOVDrawing.h"
#include "Enemy.generated.h"

/*
 * AI state that the enemy can be in
 */
UENUM()
enum class EEnemyAIState : uint8
{
	EMS_Idle							UMETA(DisplayName = "Idle"),
	EMS_LookingAround					UMETA(DisplayName = "LookingAround"),
	EMS_CheckingLastSPlayersLocation	UMETA(DisplayName = "CheckingLastPlayersLocation"),
	EMS_ChasingPlayer					UMETA(DisplayName = "ChasingPlayer"),
	EMS_ItemSearching					UMETA(DisplayName = "ItemSearching"),
	EMS_CarryingItem					UMETA(DisplayName = "CarryingItem"),
	EMS_Returning						UMETA(DisplayName = "Returning"),
	EMS_MAX								UMETA(DisplayName = "DefaultMax")	
};

/*
 * Enemy, that is chasing the player, collecting thrown items, etc
 */
UCLASS()
class SIMPLESTEALTHGAME_API AEnemy : public AGameCharacter
{
	GENERATED_BODY()

private:
	/* ====================================== */
	/* ======== AI ========================== */
	/* Enemy's AI controller */
	class AEnemyAIController* AIController;
	
	/* AI state that's enemy currently in */
	UPROPERTY(VisibleAnywhere, Category = AI, meta = (AllowPrivateAccess = "true"))
	EEnemyAIState AIState;

	/* A timer that handles being in an idle state */
	FTimerHandle IdleStateTimer;

	/* Minimum interval in seconds between looking around */
	UPROPERTY(EditInstanceOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	int32 MinLookAroundIntervalSec;

	/* Maximum interval in seconds between looking around */
	UPROPERTY(EditInstanceOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	int32 MaxLookAroundIntervalSec;	

	/*
	 * A timer to handle updating target's last seen location so we don't want have to use tick for it with
	 * loss of precision which we don't consider critical
	 */
	FTimerHandle ChasePlayerTimer;

	/* Interval in seconds for updating target's last seen location */
	UPROPERTY(EditInstanceOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	float ChasePlayerIntervalSec;

	/* A timer to look around checking last seen player's location */
	FTimerHandle CheckLastSeenPlayersLocationTimer;

	/* Time spent on last seen player location to look around, seconds */
	UPROPERTY(EditInstanceOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	float CheckLastSeenPlayerLocationTimeSec;

	/* A timer to maintain returning to spot state */
	FTimerHandle ReturningTimer;

	/* Time to adjust enemy's rotation for smooth returning to spawn location, seconds */
	UPROPERTY(EditInstanceOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	float ReturnIntervalSec;

	/* A timer for searching an item */
	FTimerHandle ItemSearchingTimer;

	/* Time to update item searching, seconds */
	UPROPERTY(EditInstanceOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	float ItemSearchingTimerIntervalSec;

	/* A timer for carrying an item */
	FTimerHandle CarryingItemTimer;

	/* Time to update movement while carrying an item, seconds */
	float CarryingItemIntervalSec;

	/* ====================================== */
	/* ======== COMBAT ====================== */
	/* Current target */
	UPROPERTY(VisibleAnywhere, Category = AI, meta = (AllowPrivateAccess = "true"))
	class AMainCharacter* TargetPlayer;

	/* Location at which enemy has last seen the player */
	UPROPERTY(VisibleAnywhere, Category = AI, meta = (AllowPrivateAccess = "true"))
	FVector TargetPlayerLastSeenLocation;

	/* ====================================== */
	/* ======== ITEMS ======================= */
	/* An item that's been noticed and needs to be carried to its original location */
	class AItem* TargetItem;

	/* Location at which enemy last heard item's hit */
	FVector TargetItemLastHeardLocation;
	
	/* ====================================== */
	/* ======== LOCATION ==================== */
	/* Enemy's initial location - location where he was spawned or placed manually */
	UPROPERTY(VisibleAnywhere, Category = Location, meta = (AllowPrivateAccess = "true"))
	FVector SpawnLocation;

	/* New rotation yaw after looking around */
	float NewRotationYaw;

	/* ====================================== */
	/* ======== MOVEMENT ==================== */
	/* Speed at which enemy chases the player */
	UPROPERTY(VisibleAnywhere, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float ChasingPlayerSpeed;

	/* Ratio to player's speed for chasing player speed */
	UPROPERTY(EditInstanceOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float ChasingPlayerSpeedRatio;

	/* Speed at which enemy returns to the original location */
	UPROPERTY(VisibleAnywhere, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float ReturningToSpawnLocationSpeed;

	/* Ration to chasing speed for returning to the original location */
	UPROPERTY(EditInstanceOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float ReturningToSpawnLocationSpeedRatio;

	/* Speed at which enemy moves when searching for a displaced item */
	UPROPERTY(VisibleAnywhere, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float ItemSearchingMovementSpeed;

	/* Ration to chasing speed for movement when searching for a displaced item */
	UPROPERTY(EditInstanceOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float ItemSearchingMovementSpeedRatio;

	/* Speed at which AI movers when carrying an item */
	UPROPERTY(VisibleAnywhere, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float CarryingItemSpeed;

	/* Ration to chasing speed for movement when carrying an item */
	UPROPERTY(EditInstanceOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float CarryingItemSpeedRatio;

	/* Rate at which enemy rotates when looking around, degrees per second */
	UPROPERTY(EditInstanceOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float LookAroundRotationRate;

	/* Current value of yaw degrees passed when looking around to make sure at least one full look around was made */
	float CurrentLookAroundValue;

	/* Current rotation to be adjusted for smoother movement */
	FRotator CurrentRotation;

	/* ====================================== */
	/* ======== PERCEPTION ================== */
	/* Enemy's perception component for sight and hearing senses */
	class UAIPerceptionComponent* PerceptionComponent;

	/* Enemy's sight sense */
	class UAISenseConfig_Sight* Sight;

	/* Enemy's sight raycast distance (actual sight radius) in meters */
	UPROPERTY(EditInstanceOnly, Category = AIPerception, meta = (AllowPrivateAccess = "true"))
	float SightRayCastDistanceMeters;

	/* For calculation purposes only */
	float SightRayCastDistanceCm;

	/* Enemy's sight FOV value in degrees */
	UPROPERTY(EditInstanceOnly, Category = AIPerception, meta = (AllowPrivateAccess = "true"))
	float SightFOVDegrees;

	/* How long enemy remembers the seen character */
	UPROPERTY(EditInstanceOnly, Category = AIPerception, meta = (AllowPrivateAccess = "true"))
	float SightMaxAgeSeconds;

	/* Enemy's hearing sense */
	class UAISenseConfig_Hearing* Hearing;

	/* Enemy's hearing range in meters */
	UPROPERTY(EditInstanceOnly, Category = AIPerception, meta = (AllowPrivateAccess = "true"))
	float HearingRangeMeters;

	UPROPERTY(EditInstanceOnly, Category = AIPerception, meta = (AllowPrivateAccess = "true"))
	float HearingMaxAgeSeconds;

	/* FOV data for drawing a debug cone */
	FEnemyFOVDrawing FOVDrawing;

public:
	// Sets default values for this character's properties
	AEnemy();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	FORCEINLINE const FVector& GetSpawnLocation() const { return SpawnLocation; }

	FORCEINLINE const EEnemyAIState& GetAIState() const { return AIState; }

	/* Set AI Controller - used when spawning from external places (i.e. spawn volumes) */
	FORCEINLINE void SetAIController(AEnemyAIController* AIControllerToSet) { AIController = AIControllerToSet; }

	FORCEINLINE const float& GetSightDistanceCm() const { return SightRayCastDistanceCm; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	/* ====================================== */
	/* ======== AI ========================== */
	/* Initialize enemy's AI controller */
	void InitAIController();

	/* Set Idle state */
	void SetIdleState();
	
	/* When in Idle state - update corresponding timer */
	void OnIdleStateTimerTick();

	/* Reset the Idle state timer and calculate new look around interval */
	void ResetIdleStateTimer();

	/* Start chasing the player */
	void SetChasePlayerState();

	/* When chasing player - update target last seen location and update enemy's rotation and move smoothly */
	void OnChasePlayerTimerTick();

	/* Reset chasing player timer */
	void ResetChasePlayerTimer();

	/* Set checking last seen player's location state */
	void SetCheckLastSeenPlayerLocationState();

	/* When checking lase seen player's location - look around */
	void OnCheckLastSeenPlayerLocationTimerTick();

	/* Reset checking last seen player's location timer */
	void ResetCheckLastSeenPlayerLocationTimer();

	/* Set returning to original spawn point state */
	void SetReturningState();

	/* When returning - update enemy's rotation to move smoothly */
	void OnReturningTimerTick();

	/* Reset returning timer tick */
	void ResetReturningTimer();

	/* Set to search item state */
	void SetItemSearchingState();

	/* Checking sought item timer tick */
	void OnItemSearchingTimerTick();

	/* Reset item searching timer */
	void ResetItemSearchingTimer();

	/* Set to carry item to its initial location */
	void SetCarryingItemState();

	/* Carrying item timer tick */
	void OnCarryingItemTimerTick();

	/* Reset carrying item timer */
	void ResetCarryingItemTimer();

	/* Set enemy's target character (player) and change the state accordingly */
	void SetTargetCharacter(AMainCharacter* TargetCharacter);

	/* Returns true if Target Player is valid as an object and is alive */
	bool IsTargetPlayerValid() const;

	/* Set enemy's target item and change the state accordingly */
	void SetTargetItem(AItem* TargetItemToSet);

	/* Clear currently active timers */
	void ClearActiveTimers();

	/* ====================================== */
	/* ======== CONSTRUCTION ================ */	
	/* Initialize default parameters */
	void InitDefaults();
	
	/* Subscribe to all necessary perception stimuli */
	void InitSubscriptions();

	/* ====================================== */
	/* ======== MOVEMENT ==================== */
	/* Calculate enemy's speed for all cases */
	void CalculateSpeed();
	
	/* Change view direction */
	void SetRandomViewDirection();

	/* Look around */
	void LookAround(const float& DeltaTime);

	/* Set current rotation of enemy's pawn */
	void SetCurrentRotation(const FRotator& RotationToSet);

	/* Adjust current rotation for smoother movement */
	void CorrectCurrentRotation(const float& DeltaTime);
	
	/* Draw an image of enemy's FOV */
	void DrawFOVImage() const;	

	/* ====================================== */
	/* ======== PERCEPTION ================== */
	/* Event that fires when target perception is updated*/
	UFUNCTION(meta = (AllowPrivateAccess = "true"))
	void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus);

	/* Event that fires when sensed actors are updated*/
	UFUNCTION(meta = (AllowPrivateAccess = "true"))
	void OnPerceptionUpdated(const TArray<AActor*>& UpdatedActors);

	/* Initialize perception */
	void InitPerception();

	/* Check if current stimulus is sight stimulus */
	bool IsSightStimulus(const FAIStimulus& Stimulus) const;

	/* Check if current stimulus is hearing stimulus */
	bool IsHearingStimulus(const FAIStimulus& Stimulus) const;

	/* Get Kismet name of the stimulating item */
	FString GetStimulatingItemName(const FAIStimulus& Stimulus) const;
};
