// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacterController.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"

void AMainCharacterController::BeginPlay()
{
	Super::BeginPlay();

	InitPauseMenu();
	InitEndGameMenu();
}

void AMainCharacterController::InitPauseMenu()
{
	if (IsValid(WPauseMenu))
	{
		PauseMenu = CreateWidget<UUserWidget>(this, WPauseMenu);
		if (IsValid(PauseMenu))
		{
			PauseMenu->AddToViewport();
			PauseMenu->SetVisibility(ESlateVisibility::Hidden);
		}		
	}
}

void AMainCharacterController::InitEndGameMenu()
{
	if (IsValid(WEndGameMenu))
	{
		EndGameMenu = CreateWidget<UUserWidget>(this, WEndGameMenu);
		if (IsValid(EndGameMenu))
		{
			EndGameMenu->AddToViewport();
			EndGameMenu->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

void AMainCharacterController::TogglePauseMenu()
{
	if (!IsValid(PauseMenu)) return;

	if (bIsPauseMenuVisible)
	{
		HidePauseMenu();
	}
	else
	{
		DisplayPauseMenu();
	}
}

void AMainCharacterController::DisplayPauseMenu_Implementation()
{
	if (!IsValid(PauseMenu)) return;

	bIsPauseMenuVisible = true;
	SetPauseMenuInputMode(true);
}

void AMainCharacterController::HidePauseMenu_Implementation()
{
	if (!IsValid(PauseMenu)) return;

	bIsPauseMenuVisible = false;
	SetPauseMenuInputMode(false);
}

void AMainCharacterController::OpenEndGameMenu_Implementation()
{
	if (!IsValid(EndGameMenu)) return;

	bShowMouseCursor = true;
	SetInputMode(FInputModeUIOnly());
}

void AMainCharacterController::CloseEndGameMenu_Implementation()
{	
	if (!IsValid(EndGameMenu)) return;

	bShowMouseCursor = false;
}

void AMainCharacterController::SetPauseMenuInputMode(const bool& bIsActive)
{
	if (bIsActive)
	{
		SetInputMode(FInputModeGameAndUI());
	}
	else
	{
		SetInputGameModeOnly();
	}

	bShowMouseCursor = bIsActive; // in Controller
	UGameplayStatics::SetGamePaused(GetWorld(), bIsActive);
}

void AMainCharacterController::SetInputGameModeOnly()
{
	SetInputMode(FInputModeGameOnly());
}
