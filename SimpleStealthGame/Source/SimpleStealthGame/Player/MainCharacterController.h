// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainCharacterController.generated.h"

/*
 * Player's controller, used mainly to hold HUD widgets
 */
UCLASS()
class SIMPLESTEALTHGAME_API AMainCharacterController final : public APlayerController
{
	GENERATED_BODY()

private:
	/* Reference to the pause menu widget in editor */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UUserWidget> WPauseMenu;

	/* Pause menu itself */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	UUserWidget* PauseMenu;

	/* Reference to the end game menu widget in editor */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = EndGameWidget, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UUserWidget> WEndGameMenu;

	/* End game menu itself */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = EndGameWidget, meta = (AllowPrivateAccess = "true"))
	UUserWidget* EndGameMenu;

	/* Is pause menu currently visible */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	bool bIsPauseMenuVisible;

public:
	/* Set input mode to Game Mode Only */
	void SetInputGameModeOnly();

	/* Switch pause menu visibility */
	void TogglePauseMenu();

	/* Show pause menu on screen */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = HUD)
	void DisplayPauseMenu();

	/* Hide pause menu from screen */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = HUD)
	void HidePauseMenu();

	/* Close end game menu - quitting to main menu or willing to play again */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = EndGameWidget)
	void OpenEndGameMenu();

	/* Close end game menu - quitting to main menu or willing to play again */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = EndGameWidget)
	void CloseEndGameMenu();

	FORCEINLINE bool IsPauseMenuVisible() const { return bIsPauseMenuVisible; }

protected:
	virtual void BeginPlay() override;

private:
	/* Init in-game pause menu */
	void InitPauseMenu();

	/* Init eng-game menu */
	void InitEndGameMenu();

	/* Set the input mode of the pause menu */
	void SetPauseMenuInputMode(const bool& bIsActive);
};
