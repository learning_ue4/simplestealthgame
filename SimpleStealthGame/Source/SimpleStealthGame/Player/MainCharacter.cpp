// Fill out your copyright notice in the Description page of Project Settings.

#include "MainCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Camera/CameraComponent.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AISense_Sight.h"
#include "SimpleStealthGame/Items/Item.h"
#include "SimpleStealthGame/Infrastructure/Logger.h"
#include "SimpleStealthGame/Settings/SSGGameUserSettings.h"
#include "SimpleStealthGame/NPC/Enemy.h"
#include "SimpleStealthGame/Player/MainCharacterController.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "SimpleStealthGame/GameGeneral/SSGGameMode.h"

// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InitDefaults();
	InitStimuliSource();
}

void AMainCharacter::InitDefaults()
{
	bIsDead = false;
	
	GetCapsuleComponent()->InitCapsuleSize(CharacterCapsuleHalfWidth, CharacterCapsuleHalfHeight);

	CharacterCapsuleHalfHeight = 105.F;
	CharacterCapsuleHalfWidth = 48.F;		

	bIsChargingThrowingPower = false;
	MaxThrowingPowerChargingTimeSec = 2.0F;
	ThrowingPowerChargingRate = .0F;

	MinThrowingDistance = 1.0F;
	MaxThrowingDistance = 15.0F;
	CurrentThrowingDistance = MinThrowingDistance;

	CombatSphereRadius = 100.0F;
	CombatSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CombatSphere"));
	CombatSphere->SetupAttachment(GetRootComponent());
	CombatSphere->InitSphereRadius(CombatSphereRadius);

	InitCamera();
}

void AMainCharacter::InitCamera()
{
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = CameraBoomLength;
	CameraBoom->bUsePawnControlRotation = true;

	GetCapsuleComponent()->InitCapsuleSize(CharacterCapsuleHalfWidth, CharacterCapsuleHalfHeight);
	
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);	
	FollowCamera->bUsePawnControlRotation = false;
}

void AMainCharacter::InitStimuliSource()
{
	StimuliSourceComponent = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("StimuliComponent"));
	StimuliSourceComponent->RegisterWithPerceptionSystem();	
	StimuliSourceComponent->RegisterForSense(UAISense_Sight::StaticClass());
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = Cast<AMainCharacterController>(GetController());

	CombatSphere->OnComponentBeginOverlap.AddDynamic(this, &AMainCharacter::OnCombatSphereOverlapBegin);

	SetThrowingPowerChargingRate();
	SetMovementSpeed();
	InitMovement();
}

void AMainCharacter::SetThrowingPowerChargingRate()
{
	if (MaxThrowingPowerChargingTimeSec <= .0F)
	{
		ULogger::LogError("Max throwing power charging time is not set properly, it should be > 0!");
		return;
	}
	
	ThrowingPowerChargingRate = MaxThrowingDistance / MaxThrowingPowerChargingTimeSec;	
}

void AMainCharacter::SetMovementSpeed()
{
	if (IsValid(GEngine))
	{
		const auto GameSettings = Cast<USSGGameUserSettings>(GEngine->GetGameUserSettings());

		if (!IsValid(GameSettings))
		{
			ULogger::LogError("Game user settings are not initialized! All movements are impossible!");
			return;
		}

		MovementSpeed = GameSettings->GetBaseGameSpeed() * GameSettings->GetGameSpeedRatio();
	}

	InitMovement();
}

void AMainCharacter::InitMovement() const
{
	auto Movement = GetCharacterMovement();
	Movement->bOrientRotationToMovement = true;
	Movement->MaxWalkSpeed = MovementSpeed;	
}

void AMainCharacter::OnCombatSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                                                AActor* OtherActor,
                                                UPrimitiveComponent* OtherComp,
                                                int32 OtherBodyIndex,
                                                bool bFromSweep,
                                                const FHitResult& SweepResult)
{
	const auto CatchingEnemy = Cast<AEnemy>(OtherActor);

	if (IsValid(CatchingEnemy))
	{
		Die();
	}
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsChargingThrowingPower)
	{
		ChargeThrowingPower(DeltaTime);
	}
}

void AMainCharacter::ChargeThrowingPower(const float& DeltaTime)
{
	const auto DeltaCharge = ThrowingPowerChargingRate * DeltaTime;
	
	if (CurrentThrowingDistance >= MaxThrowingDistance )
	{
		CurrentThrowingDistance = MaxThrowingDistance;
		return;
	}

	if (CurrentThrowingDistance < MinThrowingDistance)
	{
		CurrentThrowingDistance = MaxThrowingDistance;
	}

	CurrentThrowingDistance += DeltaCharge;	
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("LMB", IE_Pressed, this, &AMainCharacter::LMBDown);
	PlayerInputComponent->BindAction("LMB", IE_Released, this, &AMainCharacter::LMBUp);

	FInputActionBinding& PauseButtonDown = PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &AMainCharacter::PauseButtonDown);
	PauseButtonDown.bExecuteWhenPaused = true;

	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}

void AMainCharacter::MoveForward(float Value)
{
	MoveInAxis(Value, EAxis::X);
}

void AMainCharacter::MoveRight(float Value)
{
	MoveInAxis(Value, EAxis::Y);
}

void AMainCharacter::MoveInAxis(const float& MovementValue, const EAxis::Type& Axis)
{		
	if ((!IsValid(Controller)) || (MovementValue == .0F)) return;	

	const auto Rotation = Controller->GetControlRotation();
	const auto YawRotation = FRotator(.0f, Rotation.Yaw, .0F);
	
	const auto Direction = FRotationMatrix(YawRotation).GetUnitAxis(Axis);
	AddMovementInput(Direction, MovementValue);
}

void AMainCharacter::LMBDown()
{	
	if (!HasCarriedItem())
	{
		PickupItem();
		bIsChargingThrowingPower = true;
	}
}

void AMainCharacter::LMBUp()
{
	if (!HasCarriedItem())
	{
		bIsChargingThrowingPower = false;
		return;
	}
	
	ThrowItem();
	bIsChargingThrowingPower = false;
	CurrentThrowingDistance = MinThrowingDistance;
}

void AMainCharacter::ThrowItem()
{
	CarriedItem->Throw(this, CurrentThrowingDistance);
}

void AMainCharacter::PauseButtonDown()
{
	if (IsValid(PlayerController))
	{
		PlayerController->TogglePauseMenu();
	}
}

void AMainCharacter::Die()
{
	Super::Die();
	
	const auto World = GetWorld();
	if (!IsValid(World)) return;

	bIsDead = true;

	auto GameMode = Cast<ASSGGameMode>(UGameplayStatics::GetGameMode(World));

	if (!IsValid(GameMode))
	{
		ULogger::LogError("MainCharacter. Die. Game mode not found! Death is impossible. You are lucky, but the game is corrupted.");
		return;
	}
	
	GameMode->SetGameOver(this, false);
}