// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SimpleStealthGame/Common/GameCharacter.h"
#include "MainCharacter.generated.h"

UCLASS()
class SIMPLESTEALTHGAME_API AMainCharacter final : public AGameCharacter
{
	GENERATED_BODY()

private:
	/* ====================================== */
	/* ======== CAMERA ====================== */
	/* Player's camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	/* A boom arm, positioning the camera behind the player	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/* Technical player stats for movement calculations */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float CameraBoomLength;

	/* ====================================== */
	/* ======== CONTROLLER ================== */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Controller, meta = (AllowPrivateAccess = "true"))
	class AMainCharacterController* PlayerController;	
		
	/* ====================================== */
	/* ======== COMBAT AND COLLISIONS ======= */
	/* Sphere that detects interaction with enemies */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* CombatSphere;

	/* Radius of combat sphere */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float CombatSphereRadius;
	
	/* Character capsule's half height */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CollisionCapsule, meta = (AllowPrivateAccess = "true"))
	float CharacterCapsuleHalfHeight;

	/* Character capsule's half width */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CollisionCapsule, meta = (AllowPrivateAccess = "true"))
	float CharacterCapsuleHalfWidth;

	bool bIsDead;

	/* ====================================== */
	/* ======== MOVEMENT ==================== */
	/* Speed at which character moves */
	float MovementSpeed;

	/* Current player's force with which a carried item will be thrown at a certain distance in meters */
	float CurrentThrowingDistance;

	/* Min throwing distance in meters */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = PlayerStats, meta = (AllowPrivateAccess = "true"))
	float MinThrowingDistance;

	/* Max distance at which player can throw an item in meters */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = PlayerStats, meta = (AllowPrivateAccess = "true"))
	float MaxThrowingDistance;

	/* Is player currently charging throwing power */
	bool bIsChargingThrowingPower;

	/* Max time in seconds for which player can charge throwing power */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = PlayerStats, meta = (AllowPrivateAccess = "true"))
	float MaxThrowingPowerChargingTimeSec;

	/* Rate at which trowing power is charged per second */
	float ThrowingPowerChargingRate;

	/* ====================================== */
	/* ======== PERCEPTION ================== */
	/* Player stimuli to be sensed by AI */
	class UAIPerceptionStimuliSourceComponent* StimuliSourceComponent;

public:
	// Sets default values for this character's properties
	AMainCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	FORCEINLINE bool IsDead() const { return bIsDead; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/* ====================================== */
	/* ======== CAMERA ====================== */
	/* Initialize character's camera parameters */
	void InitCamera();

	/* ====================================== */
	/* ======== COMBAT ====================== */
	/* Death and related stuff */
	void Die() override;

	/* Events occured on start colliding (overlapping) with combat sphere */
	UFUNCTION()
	void OnCombatSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                                AActor* OtherActor,
	                                UPrimitiveComponent* OtherComp,
	                                int32 OtherBodyIndex,
	                                bool bFromSweep,
	                                const FHitResult& SweepResult);
	
	/* ====================================== */
	/* ======== CONSTRUCTION ================ */
	/* Initialize default parameters */
	void InitDefaults();

	/* Initialize character's movement parameters */
	void InitMovement() const;

	/* ====================================== */
	/* ======== INPUT ======================= */
	/* Pressed down to start LMBDown-action (depends on the context) */
	void LMBDown();

	/* Released to start LMBUp-action (depends on the context) */
	void LMBUp();

	/* Pressed down to Pause the game */
	void PauseButtonDown();

	/* ====================================== */
	/* ======== MOVEMENT ==================== */
	/* Set the rate at which throwing power is charged based on player's stats */
	void SetThrowingPowerChargingRate();

	/* Sets player's movement speed according to game settings */
	void SetMovementSpeed();

	/* Called for forwards/backwards input */
	void MoveForward(float MovementValue);

	/* Called for side to side input */
	void MoveRight(float MovementValue);

	/* Move character in selected axis */
	void MoveInAxis(const float& MovementValue, const EAxis::Type& Axis);

	/* Gain force to throw a carried item */
	void ChargeThrowingPower(const float& DeltaTime);

	/* Throw a carried up item */
	void ThrowItem();

	/* ====================================== */
	/* ======== PERCEPTION ================== */
	/* Init player's stimuli for AI senses */
	void InitStimuliSource();
};
