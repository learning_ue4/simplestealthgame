// Fill out your copyright notice in the Description page of Project Settings.

#include "Logger.h"
#include "Misc/FileHelper.h"
#include "HAL/PlatformFilemanager.h"
#include "Misc/Paths.h"

ULogger::ULogger()
{
	LogFilePath = "SSGLog.txt";
}

void ULogger::LogWarning(const char* Message)
{
	UE_LOG(LogTemp, Warning, TEXT("%hs"), Message);
	const auto WarningMessage = FString::Printf(TEXT("Warning:\t%hs"), Message);
	SaveToLogFile(WarningMessage);
}

void ULogger::LogWarning(FString Message)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *Message);
	const auto WarningMessage = "Warning:\t" + Message;	
	SaveToLogFile(WarningMessage);
}

void ULogger::LogError(const char* Message)
{
	UE_LOG(LogTemp, Error, TEXT("%hs"), Message);
	const auto ErrorMessage = FString::Printf(TEXT("Error:\t%hs"), Message);
	SaveToLogFile(ErrorMessage);
}

void ULogger::LogError(FString Message)
{
	UE_LOG(LogTemp, Error, TEXT("%s"), *Message);
	const auto ErrorMessage = "Error:\t" + Message;
	SaveToLogFile(ErrorMessage);
}

void ULogger::LogInfo(const char* Message)
{
	UE_LOG(LogTemp, Display, TEXT("%hs"), Message);
	const auto InfoMessage = FString::Printf(TEXT("Info:\t%hs"), Message);
	SaveToLogFile(InfoMessage);
}

void ULogger::LogInfo(FString Message)
{
	UE_LOG(LogTemp, Display, TEXT("%s"), *Message);	
	const auto InfoMessage = "Info:\t" + Message;
	SaveToLogFile(InfoMessage);
}

void ULogger::SaveToLogFile(FString Message)
{
	const auto DateTime = FDateTime::Now();
	const auto LogMessage = FString::Printf(TEXT("[%s]\t%s\n"), *(DateTime.ToString()), *Message);
	
	const auto LogFilePath = GetLogFilePath();
	FFileHelper::SaveStringToFile(LogMessage, *LogFilePath, FFileHelper::EEncodingOptions::AutoDetect, &IFileManager::Get(), EFileWrite::FILEWRITE_Append);	
}

FString ULogger::GetLogFilePath()
{
	return FPaths::ConvertRelativePathToFull(FPaths::ProjectSavedDir()) + +TEXT("/SSGLog.txt");;
}

void ULogger::CleanLogFile()
{
	const auto LogFilePath = GetLogFilePath();
	FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*LogFilePath);
}

void ULogger::LogGameStart()
{
	CleanLogFile();
	LogInfo("Game started... ");
}
