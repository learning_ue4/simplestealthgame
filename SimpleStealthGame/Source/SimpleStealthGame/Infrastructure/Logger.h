// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Logger.generated.h"

/**
 * A wrapper for UE_Log to lower the amount of code for logging
 * (UE_LOG(LogTemp, Warning... blah-blah-blah) all the time)
 */
UCLASS()
class SIMPLESTEALTHGAME_API ULogger : public UObject
{
	GENERATED_BODY()

private:
	FString LogFilePath;

private:
	static void SaveToLogFile(FString Message);

	static FString GetLogFilePath();

	static void CleanLogFile();

	static void LogInfo(const char* Message);

	static void LogInfo(FString Message);

public:
	ULogger();
	
	static void LogWarning(const char* Message);

	static void LogWarning(FString Message);

	static void LogError(const char* Message);

	static void LogError(FString Message);

	static void LogGameStart();
};
