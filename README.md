# SimpleStealthGame

A simple stealth game with basic AI made mostly using C++ with minor BP stuff.
The goal is to reach the exit zone (you won't miss it) avoiding bots by distracting their attention or simply getting past them.
Please read the Rules and Controls sections in the Main menu for more information about gameplay.

Made using UE v.4.25.

Screenshots:
![picture](scrn01.png)
![picture](scrn02.png)
![picture](scrn03.png)
![picture](scrn04.png)
